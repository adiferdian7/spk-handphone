<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_admin extends CI_Controller 
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('form_validation');
		$this->load->model('Brand_model');
		$this->load->model('User_model');
		$this->load->model('Admin_model');
		$this->load->model('Berita_model');
		$this->load->model('Jenis_model');
		$this->load->model('Spek_model');
		$this->load->model('Penilaian_model');
		$this->load->model('PenilaianHasil_model');
		$this->load->model('Ranking_model');
		$this->load->model('Data_model');
		if(!$this->session->userdata('is_login')){
			redirect('auth/index');
		}
	}

	public function aindex()
	{
		$data['title'] = 'Beranda';
		$id=$this->input->get('id');
		$data['berita'] = $this->Berita_model->getALLPonsel();
		$data['brand'] = $this->Brand_model->getALLPonsel();
		$data['spesifikasi'] =$this->Spek_model->getALL($id);
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('templates/admin_footer');
	}

	public function admin()
	{
		$data['title'] = 'Admin Nganu';
		$data['admin'] = $this->Admin_model->getALLPonsel();
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/admin', $data);
		$this->load->view('templates/admin_footer');
	}


	public function penilaian()
	{
		$data['title'] = 'Penilaian ';
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/penilaian', $data);
		$this->load->view('templates/admin_footer');
	}

	public function penilaian_simpan()
	{
		$kriterias = [
			1 => "cpu",
			2 => "memory",
			3 => "battery",
			4 => "price"
		];

		for ($i=1; $i<=4; $i++) {
			foreach ($kriterias as $k_kriteria => $v_kriteria) {
				$c[$i][$v_kriteria]	= $this->input->post("c[".$i."][".$k_kriteria."]");
			}
		}

		$this->Penilaian_model->update($c);
		redirect('Auth_admin/penilaian_hasil');
	}

	public function penilaian_hasil()
	{

		$data['title'] = 'Hasil Penilaian';
		$data["penilaian"]	= $this->Penilaian_model->all();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/perhitungan', $data);
		$this->load->view('templates/admin_footer');
	}

	public function penilaian_hasil_simpan()
	{

		$rerata = [
			"cpu" 		=> $this->input->post("cpu"),
			"memory" 	=> $this->input->post("memory"),
			"battery"	=> $this->input->post("battery"),
			"price" 	=> $this->input->post("price")
		];

		$this->PenilaianHasil_model->update($rerata);
		redirect('Auth_admin/hasil');
	}

	public function hasil()
	{
		$data['title'] = 'Hasil';
		$data["spesifikasi"] = $this->Data_model->all();
		$data["rerata"]	= $this->PenilaianHasil_model->all();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/hasil', $data);
		$this->load->view('templates/admin_footer');
	}

	public function ranking() {
		$data = $this->input->post("rank");

		$this->Ranking_model->remove();
		foreach ($data as $telephone_id => $value) {
			$this->Ranking_model->insert($value);
		}

		$data['title'] 		= 'Ranking';
		$data["ranking"] 	= $this->Ranking_model->all();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/ranking', $data);
		$this->load->view('templates/admin_footer');
	}

	public function ranking_search($harga) {
		print_r(json_encode($this->Ranking_model->search((Int) $harga)));
	}

	public function ina()
	{
		$data['title'] = 'Input Akun Admin';
		$this->form_validation->set_rules('user', 'Nama', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/input/in_admin');
			$this->load->view('templates/admin_footer');

		} else {
			$this->Admin_model->insertadmin();
			redirect('Auth_admin/admin');
		}
	}

	public function edadmin()
	{
		$data['title'] = 'Edit Admin';
		$this->form_validation->set_rules('user', 'Nama', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$id = $this->input->get('id');
			$data['data'] = $this->db->where('ida', $id)->get('admin')->row_array();
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/edit/ed_admin', $data);
			$this->load->view('templates/admin_footer');
		} else {
			redirect('admin');
		}
	}

	public function user()
	{
		$data['title'] = 'User';
		$data['user'] = $this->User_model->getALLPonsel();
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/user', $data);
		$this->load->view('templates/admin_footer');
	}

	public function eduser()
	{
		$data['title'] = 'Edit Akun User';
		$this->form_validation->set_rules('user', 'Nama', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$id = $this->input->get('id');
			$data['data'] = $this->db->where('idu', $id)->get('user')->row_array();
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/edit/ed_user', $data);
			$this->load->view('templates/admin_footer');
		} else {
			redirect('user');
		}
	}

	function hapususer()
	{
		$id=$this->input->get('id');
		$this->User_model->hapusus($id);
		redirect('auth_admin/user');
	}

	public function inu()
	{
		$data['title'] = 'Input Akun User';
		$this->form_validation->set_rules('user', 'Nama', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/input/in_user', $data);
			$this->load->view('templates/admin_footer');

		} else {
			$this->User_model->insertuser();
			redirect('Auth_admin/user');
		}
	}

	public function brand()
	{
		$data['title'] = 'Brand';
		$data['brand'] = $this->Brand_model->getALLPonsel();
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/brand', $data);
		$this->load->view('templates/admin_footer');
	}

	public function inbr()
	{
		$data['title'] = 'Brand';
		$this->form_validation->set_rules('nb', 'Brand', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/input/in_brand');
			$this->load->view('templates/admin_footer');

		} else {
			$this->Brand_model->inputbrand();
			redirect('brand');
		}
	}

	function hapusbrand()
	{

    	$id = $this->uri->segment(3);
    	$this->Brand_model->hapusbrand($id);
    	redirect('auth_admin/brand');
	}

    // save brand
	public function sav()
	{
		if ($_FILES['foto']['error'] <> 4)
		{
			$nmfile = strtolower(url_title($this->input->post('nb'))).('.png');

			/* memanggil library upload ci */
			$config['upload_path']      = './assets/gambar/logo/';
			$config['allowed_types']    = 'png';
	        $config['max_size']         = '2048'; // 2 MB
	        $config['file_name']        = $nmfile; 
	        //nama yang terupload nantinya

	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload('foto'))
	        {
          //file gagal diupload -> kembali ke form tambah
	        	$error = array('error' => $this->upload->display_errors());
	        	$this->session->set_flashdata('message', '<div class="alert alert-danger alert">'.$error['error'].'</div>');

          // $this->create();

	        }
          //file berhasil diupload -> lanjutkan ke query INSERT
	        else
	        {
	        	$foto = $this->upload->data();

	        	$this->load->library('image_lib', $config);
	        	$this->image_lib->resize();

	        	$data = array(
	        		'brand'  => $this->input->post('nb'),
	        		'foto'          => $nmfile,

	        	);
            // eksekusi query INSERT
	        	$this->Brand_model->insertb($data);
            // set pesan data berhasil dibuat
	        	$this->session->set_flashdata('message', '<div class="alert alert-success alert">
	        		Data berhasil dibuat
	        		</div>');
	        	redirect(site_url('auth_admin/brand'));
	        }
	    }
	}

	public function berita()
	{
		$data['berita'] =$this->Berita_model->getALLPonsel();
		$data['title'] = 'Berita';
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/berita',$data);
		$this->load->view('templates/admin_footer');
	}

	public function inb()
	{
		$data['title'] = 'Input Berita';
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/input/in_berita', $data);
			$this->load->view('templates/admin_footer');
		} else {
			// $this->load->view('admin/input/in_berita', $data);
			$this->Berita_model->inputberita();
			redirect('brand');
		}
	}
	public function edb()
	{
		$data['title'] = 'Edit Berita';
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		if ( $this->form_validation->run() == false ) 
		{
			$id = $this->input->get('id');
			$data['data'] = $this->db->where('idbe', $id)->get('berita')->row_array();
			$this->load->view('templates/admin_header', $data);
			$this->load->view('admin/edit/ed_berita', $data);
			$this->load->view('templates/admin_footer');
		} else {
			redirect('berita');
		}
	}

	//  save berita
	public function saveBerita()
	{
		if ($_FILES['foto']['error'] <> 4)
		{
    		// di ambil dari nama foto, yang di uploa
			$nmfile = strtolower(url_title($this->input->post('judul'))).('.jpg');

			/* memanggil library upload ci */
			$config['upload_path']      = './assets/gambar/berita/';
			$config['allowed_types']    = 'png|jpg|Jpeg';
	        $config['max_size']         = '2048'; // 2 MB
	        $config['file_name']        = $nmfile; 
	        //nama yang terupload nantinya

	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload('foto'))
	        {
          //file gagal diupload -> kembali ke form tambah
	        	$error = array('error' => $this->upload->display_errors());
	        	$this->session->set_flashdata('message', '<div class="alert alert-danger alert">'.$error['error'].'</div>');


	        }
          //file berhasil diupload -> lanjutkan ke query INSERT
	        else
	        {
	        	$foto = $this->upload->data();

	        	$this->load->library('image_lib', $config);
	        	$this->image_lib->resize();

	        	$data = array(
	        		'judul'  => $this->input->post('judul'),
	        		'isi'	=> $this->input->post('isi'),
	        		'foto'          => $nmfile,

	        	);
            // eksekusi query INSERT
	        	$this->Berita_model->insertbr($data);
            // set pesan data berhasil dibuat
	        	$this->session->set_flashdata('message', '<div class="alert alert-success alert">
	        		Data berhasil dibuat
	        		</div>');
	        	redirect(site_url('auth_admin/berita'));
	        }
	    }
	}

	//  save berita
	public function updateberita()
	{
		$link = FCPATH.'assets/gambar/berita/';
    	// echo $link;die();
		if ($_FILES['foto']['error'] <> 4)
		{
			$data = $this->db->where('idbe',$this->input->post('id'))->get('berita')->row_array();
			unlink($link.$data['foto']);
    		// di ambil dari nama foto, yang di uploa
			$nmfile = $this->input->post('id');

			/* memanggil library upload ci */
			$config['upload_path']      = './assets/gambar/berita/';
			$config['allowed_types']    = 'png|jpg|jpeg';
	        $config['max_size']         = '2048'; // 2 MB
	        $config['file_name']        = $nmfile; 
	        //nama yang terupload nantinya

	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload('foto'))
	        {
          //file gagal diupload -> kembali ke form tambah
	        	$error = array('error' => $this->upload->display_errors());
	        	$this->session->set_flashdata('message', '<div class="alert alert-danger alert">'.$error['error'].'</div>');
	        	echo"gagal upload";
          // $this->create();

	        }
          //file berhasil diupload -> lanjutkan ke query INSERT
	        else
	        {
	        	$data = array(
	        		'judul'  => $this->input->post('judul'),
	        		'isi'	=> $this->input->post('isi'),
	        		'foto'  => $this->upload->data('file_name'),

	        	);
            // eksekusi query INSERT
	        	$this->Berita_model->updatebr($data, $this->input->post('id'));
            // set pesan data berhasil dibuat
	        	$this->session->set_flashdata('message', '<div class="alert alert-success alert">
	        		Data berhasil diubah
	        		</div>');
	        	redirect(site_url('auth_admin/berita'));
	        }
	    }else{
	    	$data = array(
	    		'judul'  => $this->input->post('judul'),
	    		'isi'	=> $this->input->post('isi'),

	    	);
            // eksekusi query INSERT
	    	$this->Berita_model->updatebr($data, $this->input->post('id'));
            // set pesan data berhasil dibuat
	    	$this->session->set_flashdata('message', '<div class="alert alert-success alert">
	    		Data berhasil diubah
	    		</div>');
	    	redirect(site_url('auth_admin/berita'));
	    }
	}

	function hapusberita()
	{
		$id=$this->input->get('id');
		$this->Berita_model->hapusbr($id);
		redirect('auth_admin/berita');
	}

	public function spek()
	{
		$data['title'] = 'Spesifikasi';
		$id=$this->input->get('id');
		$data['spesifikasi'] =$this->Spek_model->getALL($id);
		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/spek',$data);
		$this->load->view('templates/admin_footer');
	}

	public function ins()
	{
		$data['title'] = 'Ponsel';
		$data['jenis'] = $this->Jenis_model->getALLPonsel();
		$data['brand'] = $this->Brand_model->getALLPonsel();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('admin/input/in_spesifikasi', $data);
		$this->load->view('templates/admin_footer');
	}

	public function savespesifikasi(){
		$save['idb'] 			= $this->input->post('brand');
		$save['ida'] 			= 1;
		$save['idj'] 			= $this->input->post('kategori');
		$save['name'] 			= $this->input->post('nama');

		// $save['foto'] 	= 'foto.jpg';
		$save['network'] 		= json_encode($this->input->post('jaringan'));
		$save['launching'] 		= $this->input->post('tahun');
		$save['dimensi'] 		= $this->input->post('dimensi');
		$save['berat'] 			= $this->input->post('berat');
		$save['bahan'] 			= $this->input->post('bahan');
		$save['kartu'] 			= $this->input->post('sim');
		$save['panel'] 			= $this->input->post('panel');
		$save['ukuran'] 		= $this->input->post('ukuran');
		$save['resolusi'] 		= $this->input->post('resolusi');
		$save['proteksi'] 		= $this->input->post('proteksi');
		$save['platform'] 		= $this->input->post('platform');
		$save['cpu'] 			= $this->input->post('cpu');
		$save['slot'] 			= $this->input->post('slot');
		$save['kapasitas'] 		= json_encode($this->input->post('memory'));
		$save['ram'] 			= $this->input->post('ram');
		$save['main_camera'] 	= $this->input->post('utama');
		$save['front_camera'] 	= $this->input->post('selfi');
		$save['loundspeker'] 	= $this->input->post('suara');
		$save['jack'] 			= $this->input->post('jack');
		$save['wlan'] 			= $this->input->post('wifi');
		$save['bluetooth'] 		= $this->input->post('bluetooth');
		$save['gps'] 			= $this->input->post('gps');
		$save['nfc'] 			= $this->input->post('nfc');
		$save['infrared'] 		= $this->input->post('infrared');
		$save['radio'] 			= $this->input->post('radio');
		$save['usb'] 			= $this->input->post('usb');
		$save['fitur'] 			= json_encode($this->input->post('tambahan'));
		$save['tambahan'] 		= json_encode($this->input->post('lainya'));
		$save['battery'] 		= $this->input->post('baterai');
		$save['qc'] 			= $this->input->post('qc');
		$save['harga'] 			= $this->input->post('harga');

		//UPLOAD FOTO
		$nmfile = strtolower(url_title($this->input->post('nama'))).('.jpg');
		$config['upload_path']      = './assets/gambar/ponsel/';
		$config['allowed_types']    = 'png|jpg|Jpeg';
        $config['max_size']         = '2048'; // 2 MB
        $config['file_name']        = $nmfile; 
        //nama yang terupload nantinya

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto'))
        {
      		//file gagal diupload -> kembali ke form tambah
        	$error = array('error' => $this->upload->display_errors());
        	$this->session->set_flashdata('message', '<div class="alert alert-danger alert">'.$error['error'].'</div>');


        }
        $save['foto'] 	= 'assets/gambar/ponsel/'.$nmfile;

        $this->db->insert('spesifikasi', $save);
        $simpan["spesifikasi_id"]	= $this->db->insert_id();


        $cpu 		= $save['cpu'];
        $cpu_number	= preg_replace("/[a-zA-Z ]/i", "", $cpu);
        if (preg_match("/snapdragon/i", $cpu)) {
        	if (400 <= $cpu_number && $cpu_number <= 460) {
        		$simpan["cpu"] = 1;
        	}
        	else if (600 <= $cpu_number && $cpu_number <= 675) {
        		$simpan["cpu"] = 2;
        	}
        	else if (710 <= $cpu_number && $cpu_number <= 765) {
        		$simpan["cpu"] = 3;
        	}
        	else if (801 <= $cpu_number && $cpu_number <= 865) {
        		$simpan["cpu"] = 4;
        	}
        }
        else if (preg_match("/mediatek/i", $cpu)) {
        	if ($cpu_number <= 30) {
        		$simpan["cpu"] = 4;
        	}
        	else if (35 <= $cpu_number && $cpu_number <= 90) {
        		$simpan["cpu"] = 3;
        	}
        	else if (6750 <= $cpu_number && $cpu_number <= 6753) {
        		$simpan["cpu"] = 2;
        	}
        	else if (6592 <= $cpu_number && $cpu_number <= 6739) {
        		$simpan["cpu"] = 1;
        	}
        }
        else if (preg_match("/kirin/i", $cpu)) {
        	if (620 <= $cpu_number && $cpu_number <= 659) {
        		$simpan["cpu"] = 1;
        	}
        	else if (710 <= $cpu_number && $cpu_number <= 910) {
        		$simpan["cpu"] = 2;
        	}
        	else if (920 <= $cpu_number && $cpu_number <= 935) {
        		$simpan["cpu"] = 3;
        	}
        	else if (950 <= $cpu_number && $cpu_number <= 990) {
        		$simpan["cpu"] = 4;
        	}
        }
        else if (preg_match("/exynos/i", $cpu)) {
        	if (7270 <= $cpu_number && $cpu_number <= 7880) {
        		$simpan["cpu"] = 1;
        	}
        	else if (7884 <= $cpu_number && $cpu_number <= 7904) {
        		$simpan["cpu"] = 2;
        	}
        	else if (9110 <= $cpu_number && $cpu_number <= 9611) {
        		$simpan["cpu"] = 3;
        	}
        	else {
        		$simpan["cpu"] = 4;
        	}
        }
        else if (preg_match("/apple/i", $cpu)) {
        	if (4 <= $cpu_number && $cpu_number <= 7) {
        		$simpan["cpu"] = 1;
        	}
        	else if (8 <= $cpu_number && $cpu_number <= 9) {
        		$simpan["cpu"] = 2;
        	}
        	else if (10 <= $cpu_number && $cpu_number <= 10) {
        		$simpan["cpu"] = 3;
        	}
        	else if (11 <= $cpu_number && $cpu_number <= 13) {
        		$simpan["cpu"] = 4;
        	}
        }

        if (1 <= $save['ram'] && $save['ram'] <= 2) {
        	$simpan["memory"] = 1;
        }
        else if (3 <= $save['ram'] && $save['ram'] <= 4) {
        	$simpan["memory"] = 2;
        }
        else if (6 <= $save['ram'] && $save['ram'] <= 8) {
        	$simpan["memory"] = 3;
        }
        else if (10 <= $save['ram'] && $save['ram'] <= 12) {
        	$simpan["memory"] = 4;
        }

        if (0 <= $save['battery'] && $save['battery'] <= 2500) {
        	$simpan["battery"] = 1;
        }
        else if (2501 <= $save['battery'] && $save['battery'] <= 4000) {
        	$simpan["battery"] = 2;
        }
        else if (4001 <= $save['battery'] && $save['battery'] <= 5000) {
        	$simpan["battery"] = 3;
        }
        else if (5000 < $save['battery']) {
        	$simpan["battery"] = 4;
        }

        if (0 <= $save['harga'] && $save['harga'] <= 3000000) {
        	$simpan["price"] = 1;
        }
        else if (3000001 <= $save['harga'] && $save['harga'] <= 6000000) {
        	$simpan["price"] = 2;
        }
        else if (6000001 <= $save['harga'] && $save['harga'] <= 10000000) {
        	$simpan["price"] = 3;
        }
        else if (10000000 < $save['harga']) {
        	$simpan["price"] = 4;
        }
        $this->db->insert("data", $simpan);


        $this->session->set_flashdata('message', '<div class="alert alert-success alert">
        	Data berhasil dibuat
        	</div>');

        redirect(site_url('auth_admin/ins',$data));

    }

	//  save berita
    public function editspek()
    {
    	$data['title'] = 'Edit Spesifikasi';
    	$id = $this->uri->segment(3);
    	$data['sex'] = $id;
    	//query select dari table 
    	$data['brand'] = $this->Brand_model->getALLPonsel();
    	$data['jenis'] = $this->Jenis_model->getALLPonsel();
    	$data['spek'] = $this->Spek_model->getALLPonsel_spek($id);
    	$this->load->view('templates/admin_header', $data);
    	$this->load->view('admin/edit/ed_spesifikasi',$data);
    	$this->load->view('templates/admin_footer');	
    }

		//  save berita
    public function saveditspek()
    {
    	$link 	= FCPATH.'assets/gambar/ponsel/';
    	$id 	= $this->input->post('idspek');

    	$nmfile 					= strtolower(url_title($this->input->post('nama'))).('.jpg');
    	$config['upload_path']      = './assets/gambar/ponsel/';
    	$config['allowed_types']    = 'png|jpg|Jpeg';
        $config['max_size']         = '2048'; // 2 MB
        $config['file_name']        = $nmfile;

        $this->load->library('upload', $config);
        $this->upload->do_upload("foto");
        $foto = $this->upload->data();

        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        $id = $this->input->post('idspek');
        $data = array(
        	'idb' 	=> $this->input->post('brand'),
        	'ida' 	=> 1,
        	'idj' 	=> $this->input->post('jenis'),
        	'name' 	=> $this->input->post('nama'),
        	'foto' 	=> 'assets/gambar/ponsel/'.$nmfile,

        	'network' 		=> json_encode($this->input->post('jaringan')),
        	'launching'		=> $this->input->post('tahun'),
        	'dimensi' 		=> $this->input->post('dimensi'),
        	'berat' 		=> $this->input->post('berat'),
        	'bahan' 		=> $this->input->post('bahan'),
        	'kartu' 		=> $this->input->post('sim'),
        	'panel' 		=> $this->input->post('panel'),
        	'ukuran' 		=> $this->input->post('ukuran'),
        	'resolusi' 		=> $this->input->post('resolusi'),
        	'proteksi' 		=> $this->input->post('proteksi'),
        	'platform' 		=> $this->input->post('platform'),
        	'cpu' 			=> $this->input->post('cpu'),
        	'slot' 			=> $this->input->post('slot'),
        	'kapasitas' 	=> json_encode($this->input->post('memory')),
        	'ram' 			=> $this->input->post('ram'),
        	'main_camera' 	=> $this->input->post('utama'),
        	'front_camera' 	=> $this->input->post('selfi'),
        	'loundspeker' 	=> $this->input->post('suara'),
        	'jack' 			=> $this->input->post('jack'),
        	'wlan' 			=> $this->input->post('wifi'),
        	'bluetooth'	=> $this->input->post('bluetooth'),
        	'gps' 	=> $this->input->post('gps'),
        	'nfc' 	=> $this->input->post('nfc'),
        	'infrared' 	=> $this->input->post('infrared'),
        	'radio' 	=> $this->input->post('radio'),
        	'usb'	=> $this->input->post('usb'),
        	'fitur'	=> json_encode($this->input->post('tambahan')),
        	'tambahan' 	=> json_encode($this->input->post('lainya')),
        	'battery' 	=> $this->input->post('baterai'),
        	'qc' 	=> $this->input->post('qc'),
        	'harga' 	=> $this->input->post('harga'),
        );
        // eksekusi query INSERT
        $this->Spek_model->updatespekcuk($data, $id);



        $cpu 		= $data['cpu'];
        $cpu_number	= preg_replace("/[a-zA-Z ]/i", "", $cpu);
        if (preg_match("/snapdragon/i", $cpu)) {
        	if (400 <= $cpu_number && $cpu_number <= 460) {
        		$simpan["cpu"] = 1;
        	}
        	else if (600 <= $cpu_number && $cpu_number <= 675) {
        		$simpan["cpu"] = 2;
        	}
        	else if (710 <= $cpu_number && $cpu_number <= 765) {
        		$simpan["cpu"] = 3;
        	}
        	else if (801 <= $cpu_number && $cpu_number <= 865) {
        		$simpan["cpu"] = 4;
        	}
        }
        else if (preg_match("/mediatek/i", $cpu)) {
        	if ($cpu_number <= 30) {
        		$simpan["cpu"] = 4;
        	}
        	else if (35 <= $cpu_number && $cpu_number <= 90) {
        		$simpan["cpu"] = 3;
        	}
        	else if (6750 <= $cpu_number && $cpu_number <= 6753) {
        		$simpan["cpu"] = 2;
        	}
        	else if (6592 <= $cpu_number && $cpu_number <= 6739) {
        		$simpan["cpu"] = 1;
        	}
        }
        else if (preg_match("/kirin/i", $cpu)) {
        	if (620 <= $cpu_number && $cpu_number <= 659) {
        		$simpan["cpu"] = 1;
        	}
        	else if (710 <= $cpu_number && $cpu_number <= 910) {
        		$simpan["cpu"] = 2;
        	}
        	else if (920 <= $cpu_number && $cpu_number <= 935) {
        		$simpan["cpu"] = 3;
        	}
        	else if (950 <= $cpu_number && $cpu_number <= 990) {
        		$simpan["cpu"] = 4;
        	}
        }
        else if (preg_match("/exynos/i", $cpu)) {
        	if (7270 <= $cpu_number && $cpu_number <= 7880) {
        		$simpan["cpu"] = 1;
        	}
        	else if (7884 <= $cpu_number && $cpu_number <= 7904) {
        		$simpan["cpu"] = 2;
        	}
        	else if (9110 <= $cpu_number && $cpu_number <= 9611) {
        		$simpan["cpu"] = 3;
        	}
        	else {
        		$simpan["cpu"] = 4;
        	}
        }
        else if (preg_match("/apple/i", $cpu)) {
        	if (4 <= $cpu_number && $cpu_number <= 7) {
        		$simpan["cpu"] = 1;
        	}
        	else if (8 <= $cpu_number && $cpu_number <= 9) {
        		$simpan["cpu"] = 2;
        	}
        	else if (10 <= $cpu_number && $cpu_number <= 10) {
        		$simpan["cpu"] = 3;
        	}
        	else if (11 <= $cpu_number && $cpu_number <= 13) {
        		$simpan["cpu"] = 4;
        	}
        }

        if (1 <= $data['ram'] && $data['ram'] <= 2) {
        	$simpan["memory"] = 1;
        }
        else if (3 <= $data['ram'] && $data['ram'] <= 4) {
        	$simpan["memory"] = 2;
        }
        else if (6 <= $data['ram'] && $data['ram'] <= 8) {
        	$simpan["memory"] = 3;
        }
        else if (10 <= $data['ram'] && $data['ram'] <= 12) {
        	$simpan["memory"] = 4;
        }

        if (0 <= $data['battery'] && $data['battery'] <= 2500) {
        	$simpan["battery"] = 1;
        }
        else if (2501 <= $data['battery'] && $data['battery'] <= 4000) {
        	$simpan["battery"] = 2;
        }
        else if (4001 <= $data['battery'] && $data['battery'] <= 5000) {
        	$simpan["battery"] = 3;
        }
        else if (5000 < $save['battery']) {
        	$simpan["battery"] = 4;
        }

        if (0 <= $data['harga'] && $data['harga'] <= 3000000) {
        	$simpan["price"] = 1;
        }
        else if (3000001 <= $data['harga'] && $data['harga'] <= 6000000) {
        	$simpan["price"] = 2;
        }
        else if (6000001 <= $data['harga'] && $data['harga'] <= 10000000) {
        	$simpan["price"] = 3;
        }
        else if (10000000 < $data['harga']) {
        	$simpan["price"] = 4;
        }
        
        $this->Spek_model->updatespekcok($simpan, $id);


        // set pesan data berhasil dibuat
        $this->session->set_flashdata('message', '<div class="alert alert-success alert">
        	Data berhasil dibuat
        	</div>');

        redirect(site_url('auth_admin/spek'));
    }

    function hapuspek()
    {

    	$id = $this->uri->segment(3);
    	$this->Spek_model->hapuspek($id);
    	redirect('auth_admin/spek');
    }

    public function cari (){
    	$data = $this->input->post('brand');
    	$data['spesifikasi']=$this->AHP_model->cari($keyword);

    	$this->load->view('templates/admin_header', $data);
    	$this->load->view('admin/hasil', $data);
    	$this->load->view('templates/admin_footer');
    }
}



