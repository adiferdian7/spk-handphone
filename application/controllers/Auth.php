<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('form_validation');
		$this->load->model('Brand_model');
		$this->load->model('User_model');
		$this->load->model('Admin_model');
		$this->load->model('Berita_model');
		$this->load->model('Jenis_model');
		$this->load->model('Spek_model');
		$this->load->model('Ranking_model');
		$this->load->model('Data_model');
		$this->load->model('RankingCustom_model');
	}

	public function coba($id){
		$data['title'] = 'Halaman Utama';
		$all['data'] = $this->Spek_model->coba($id);
		$this->load->view('templates/auth_header', $data);
		$this->load->view('coba', $all);
		$this->load->view('templates/auth_footer');
	}

	public function index()
	{
		$data['title'] = 'Halaman Utama';
		$id = $this->input->get('id');
		$data['brand'] = $this->Brand_model->getALLPonsel();
		$data['berita'] = $this->Berita_model->getLastPonsel();
		$data['spesifikasi'] = $this->Spek_model->getALL($id);
		$all = $this->Berita_model->getALLPonsel();
		$this->load->view('templates/auth_header', $data);
		$this->load->view('index', $data);
		$this->load->view('templates/auth_footer');
	}
	public function check_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$exist = $this->db->where('username',$username)->get('admin');

		if($exist->num_rows() > 0 ){
			$data = $exist->row_array();
			if($data['pass'] == $password){
				$session['is_login'] = TRUE;
				$session['username'] = $username;
				$this->session->set_userdata($session);
				redirect(site_url('auth_admin/aindex'));
			}else{
				//user dan password tidak sesuai
				redirect(site_url('auth/index'));
			}
		}else{
			//user tidak ditemukan
			redirect(site_url('auth/index'));
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url('auth/index'));
	}

	public function berita()
	{
		$data['title'] = 'Halaman Berita';
		$id = $this->input->get('id');
		$data['vberita'] = $this->Berita_model->getALLPonsel();
		$this->load->view('templates/auth_header', $data);
		$this->load->view('berita', $data);
		$this->load->view('templates/auth_footer');
	}


    function create_captcha() {
        $options = array(
            "img_path" => "./assets/captcha/",
            "img_url" => base_url("assets/captcha/"),
            "img_width" => "150",
            "img_height" => "30",
			'word_length'   => 5
        );
        $captcha 	= create_captcha($options);

		$this->session->unset_userdata("captchaword");
		$this->session->set_userdata("captchaword", $captcha["word"]);

        return $captcha["image"];
	}

	function check_captcha() {
        if ($this->input->post("captcha") === $this->session->userdata("captchaword")) {
			return true;
		}
		else {
			$this->form_validation->set_message("check_captcha", "Captcha is wrong.");
			return false;
		}
	}

	public function pencarian()
	{
		$data['title'] = 'Halaman Pencarian';
		$data['brand'] = $this->Brand_model->getALLPonsel();
		$data['jenis'] = $this->Jenis_model->getALLPonsel();
		$data['captcha'] = $this->create_captcha();

		$this->load->view('templates/auth_header', $data);
		$this->load->view('Pencarian', $data);
		$this->load->view('templates/auth_footer');
	}

	public function review()
	{
		$data['title'] = 'Halaman Review';
		$this->load->view('templates/auth_header', $data);
		$this->load->view('review');
		$this->load->view('templates/auth_footer');
	}

	public function brand()
	{
		// $y = 1%4;
		// echo $y;
		// die();
		$data['title'] = 'Halaman Brand';
		$data['brand'] = $this->Brand_model->getALLPonsel();
		$this->load->view('templates/auth_header', $data);
		$this->load->view('brand', $data);
		$this->load->view('templates/auth_footer');
	}

	public function list()
	{
		$id = $this->uri->segment(3);
		$data['title'] = 'Halaman Daftar Ponsel';
		$data['brand'] = $this->Brand_model->getBrandid($id);
		$data['spesifikasi'] = $this->Brand_model->getALLPonsel_all_spek($id);
		$this->load->view('templates/auth_header', $data);
		$this->load->view('listhp', $data);
		$this->load->view('templates/auth_footer');
	}

	public function register()
	{
		$data['title'] = 'Registrasi';
		$this->load->view('templates/auth_header', $data);
		$this->load->view('register');
		$this->load->view('templates/auth_footer');
	}

	public function reg(){
		if( $this->form_validation->run() == false) {
			$this->load->view('templates/auth_header');
			$this->load->view('register');
			$this->load->view('templates/auth_footer');

		}
	}

	public function vberita(){
		$id = $this->input->get('id');
		$data['title'] = 'Registrasi';
		$data['berita'] = $this->Berita_model->getBeritaid($id);
		$data['vberita'] = $this->Berita_model->getBeritaid($id);
		$this->load->view('templates/auth_header', $data);
		$this->load->view('konten_berita', $data);
		$this->load->view('templates/auth_footer');
	}

	public function spek()
	{
		$data['title'] = 'Halaman Spesifikasi';
		$id = $this->uri->segment(3);
		$data['spesifikasi'] = $this->Spek_model->getALLPonsel_spek($id);
		$data['brand'] = $this->Brand_model->getALLPonsel();
		$data['id'] = $id;

		if (count($data["spesifikasi"]) > 0) {
			$data_spesifikasi = [
				"s1" => "Fingerprint",
				"s2" => "Barometer",
				"s3" => "Gyro",
				"s4" => "Compas",
				"s5" => "Proximty",
				"s6" => "Accelerometer",
				"s7" => "FaceID",
				"s8" => "Whireles Charging",
			];
			foreach (json_decode($data['spesifikasi'][0]["fitur"], true) as $key => $value) {
				$fitur[] = $data_spesifikasi[$value];
			}
			$data['spesifikasi'][0]["fitur"] = join(", ", $fitur);

			$data_memory = [
				"k1" => "16 GB",
				"k2" => "32 GB",
				"k3" => "64 GB",
				"k4" => "128 GB",
				"k5" => "256 GB",
				"k6" => "512 GB",
				"k7" => "1 TB",
				"s8" => "Whireles Charging",
			];
			foreach (json_decode($data['spesifikasi'][0]["kapasitas"], true) as $key => $value) {
				$memory[] = $data_memory[$value];
			}
			$data['spesifikasi'][0]["kapasitas"] = join(", ", $memory);
		}
		//exit;

		$this->load->view('templates/auth_header', $data);
		$this->load->view('spesifikasi', $data);
		$this->load->view('templates/auth_footer');
	}

	public function search()
	{
		
		$keyword = $this->input->post('keyword');
		$data['products']=$this->product_m->cari($keyword);
		$this->load->view('templates/auth_header', $data);
		$this->load->view('spesifikasi', $data);
		$this->load->view('templates/auth_footer');
	}

	public function rekomendasi()
	{
		$data['data'] 	= $this->Ranking_model->all();
		$data['title'] 	= 'Halaman Rekomendasi';

		$this->load->view('templates/auth_header', $data);
		$this->load->view('rekomendasi', $data);
		$this->load->view('templates/auth_footer');
	}

	public function rekomendasi_custom()
	{
		if ($this->input->post('submit') === "Hitung") {
			$kriterias = [
				1 => "cpu",
				2 => "memory",
				3 => "battery",
				4 => "price"
			];

			for ($i=1; $i<=4; $i++) {
				foreach ($kriterias as $k_kriteria => $v_kriteria) {
					$c[$i][$v_kriteria]	= $this->input->post("c[".$i."][".$k_kriteria."]");
				}

				@$j_cpu     += $c[$i]["cpu"];
				@$j_memory  += $c[$i]["memory"];
				@$j_battery += $c[$i]["battery"];
				@$j_price   += $c[$i]["price"];
			}

			//eigen jumlan dan rerata
			for ($i=1; $i<=4; $i++) {
				$ej[$i] = ($c[$i]["cpu"]/$j_cpu) + ($c[$i]["memory"]/$j_memory) + ($c[$i]["battery"]/$j_battery) + ($c[$i]["price"]/$j_price);
				$er[$i]	= $ej[$i]/4;
			}

			$data["spesifikasi"] = $this->Data_model->all();
			foreach ($data["spesifikasi"] as $key_spe => $value_spe) {
				@$spe_j_cpu     += $value_spe["cpu"];
				@$spe_j_memory  += $value_spe["memory"];
				@$spe_j_battery += $value_spe["battery"];
				@$spe_j_price   += $value_spe["price"];
			}

			$this->RankingCustom_model->remove();
			foreach ($data["spesifikasi"] as $key_spe => $value_spe) {
				$rekom[$key_spe][1]	= ($value_spe["cpu"]/$spe_j_cpu)*$er[1];
				$rekom[$key_spe][2]	= ($value_spe["memory"]/$spe_j_memory)*$er[2];
				$rekom[$key_spe][3]	= ($value_spe["battery"]/$spe_j_battery)*$er[3];
				$rekom[$key_spe][4]	= ($value_spe["price"]/$spe_j_price)*$er[4];

				$hasil	= array(
					"telephone_id" => $value_spe["spesifikasi_id"],
					"value" => array_sum($rekom[$key_spe])
				);
				$this->RankingCustom_model->insert($hasil);
			}

			$data['title'] 		= 'Custom Rekomendasi';
			$data["ranking"] 	= $this->RankingCustom_model->all();

			$this->load->view('templates/auth_header', $data);
			$this->load->view('rekomendasi_custom_rank', $data);
			$this->load->view('templates/auth_footer');
		}
		else {
			$data['data'] 	= $this->Ranking_model->all();
			$data['title'] 	= 'Custom Rekomendasi';

			$this->load->view('templates/auth_header', $data);
			$this->load->view('rekomendasi_custom', $data);
			$this->load->view('templates/auth_footer');
		}
	}

	public function rekomendasi_search($harga) {
		print_r(json_encode($this->Ranking_model->search((Int) $harga)));
	}

	public function rekomendasi_manual_search($harga) {
		print_r(json_encode($this->RankingCustom_model->search((Int) $harga)));
	}

	public function search_manual() {
		$this->form_validation->set_rules("captcha", "Captcha", "trim|callback_check_captcha|required");
        if ($this->form_validation->run() === true) {
			$search["brand"]	= $this->input->post("brand");
			$search["cpu"] 		= $this->input->post("cpu");
			$search["kamera"] 	= $this->input->post("kamera");
			$search["memory"] 	= $this->input->post("memory");
			$search["battery"] 	= (Int) $this->input->post("battery");
			$search["price"]	= (Int) $this->input->post("price");
	
			$data['data'] 	= $this->Ranking_model->all_manual($search);
	
			$data['title'] 	= 'Halaman Pencarian';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('search_manual', $data);
			$this->load->view('templates/auth_footer');
        }
        else {
            redirect(base_url()."auth/pencarian");
        }
	}

	public function cari(){
		$search["name"]	= $this->input->post("name");
		$data['data'] 	= $this->Ranking_model->cari($search);

		$data['title'] 	= 'Halaman Pencarian';
		$this->load->view('templates/auth_header', $data);
		$this->load->view('search_manual', $data);
		$this->load->view('templates/auth_footer');
	}
}
