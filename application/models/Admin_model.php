<?php 


class Admin_model extends CI_Model
{
	public function getALLPonsel()
	{
		$query = $this->db->get('admin');
		return $query->result_array();
	}

	public function insertadmin()
	{
		$data = [
			"username" => $this->input->post('user'), //kanan nama dari field
			"pass" => $this->input->post('pass'), //kiri nama dari tabel
			"telefon" => $this->input->post('tele'),
			"email" => $this->input->post('email')
		];

		$this->db->insert('admin', $data);
	}
}