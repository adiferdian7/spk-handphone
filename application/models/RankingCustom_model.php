<?php 
class RankingCustom_model extends CI_Model
{
	public $table = "ranking_custom";

	function all()
	{
		$this->db->select('*');
		$this->db->from('ranking_custom');
		$this->db->join('spesifikasi', 'spesifikasi.ids = ranking_custom.telephone_id');
		$this->db->order_by("ranking_custom.value", "DESC");

		return $this->db->get()->result_array();
	}

	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	function remove()
	{
		$this->db->empty_table($this->table);
	}

	function search($harga)
	{
		if ($harga === 1) {
			$min	= 0;
			$max	= 3000000;
		}
		elseif ($harga === 2) {
			$min	= 3000001;
			$max	= 6000000;
		}
		elseif ($harga === 3) {
			$min	= 6000001;
			$max	= 10000000;
		}
		elseif ($harga === 4) {
			$min	= 10000000;
			$max	= 13333337777777;
		}
		else {
			$min	= 0;
			$max	= 13333337777777;
		}

		$this->db->select('*');
		$this->db->from('ranking_custom');
		$this->db->join('spesifikasi', 'spesifikasi.ids = ranking_custom.telephone_id');
		$this->db->order_by("ranking_custom.value", "DESC");
		$this->db->where('spesifikasi.harga >=', $min);
		$this->db->where('spesifikasi.harga <=', $max);

		return $this->db->get()->result_array();
	}
}