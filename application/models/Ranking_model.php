<?php 
class Ranking_model extends CI_Model
{
	public $table = "ranking";

	function all()
	{
		$this->db->select('*');
		$this->db->from('ranking');
		$this->db->join('spesifikasi', 'spesifikasi.ids = ranking.telephone_id');
		$this->db->order_by("ranking.value", "DESC");

		return $this->db->get()->result_array();
	}

	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}

	function remove()
	{
		$this->db->empty_table($this->table);
	}

	function search($harga)
	{
		if ($harga === 1) {
			$min	= 0;
			$max	= 3000000;
		}
		elseif ($harga === 2) {
			$min	= 3000001;
			$max	= 6000000;
		}
		elseif ($harga === 3) {
			$min	= 6000001;
			$max	= 10000000;
		}
		elseif ($harga === 4) {
			$min	= 10000000;
			$max	= 13333337777777;
		}
		else {
			$min	= 0;
			$max	= 13333337777777;
		}

		$this->db->select('*');
		$this->db->from('ranking');
		$this->db->join('spesifikasi', 'spesifikasi.ids = ranking.telephone_id');
		$this->db->order_by("ranking.value", "DESC");
		$this->db->where('spesifikasi.harga >=', $min);
		$this->db->where('spesifikasi.harga <=', $max);

		return $this->db->get()->result_array();
	}

	function cari($data){
		// print_r($data);
		// exit();
		$this->db->select('*');
		$this->db->from('spesifikasi');
		$this->db->like('spesifikasi.name', $data["name"]);
		return $this->db->get()->result_array();
	}

	function all_manual($data)
	{
		$battery = $data["battery"];
		if ($battery === 1) {
			$battery_min	= 2000;
			$battery_max	= 3000;
		}
		elseif ($battery === 2) {
			$battery_min	= 3001;
			$battery_max	= 4000;
		}
		elseif ($battery === 3) {
			$battery_min	= 4001;
			$battery_max	= 5000;
		}
		elseif ($battery === 4) {
			$battery_min	= 5000;
			$battery_max	= 13333337777777;
		}
		else {
			$battery_min	= 0;
			$battery_max	= 13333337777777;
		}

		$price = $data["price"];
		if ($price === 1) {
			$price_min	= 0;
			$price_max	= 3000000;
		}
		elseif ($price === 2) {
			$price_min	= 3000001;
			$price_max	= 6000000;
		}
		elseif ($price === 3) {
			$price_min	= 6000001;
			$price_max	= 10000000;
		}
		elseif ($price === 4) {
			$price_min	= 10000000;
			$price_max	= 13333337777777;
		}
		else {
			$price_min	= 0;
			$price_max	= 13333337777777;
		}

		$this->db->select('*');
		$this->db->from('ranking');
		$this->db->join('spesifikasi', 'spesifikasi.ids = ranking.telephone_id');
		$this->db->order_by("ranking.value", "DESC");
		$this->db->where('spesifikasi.battery >=', $battery_min);
		$this->db->where('spesifikasi.battery <=', $battery_max);
		$this->db->where('spesifikasi.harga >=', $price_min);
		$this->db->where('spesifikasi.harga <=', $price_max);
		if (!empty($data["memory"])) {
			$this->db->where('spesifikasi.ram', $data["memory"]);
		}
		if (!empty($data["cpu"])) {
			$this->db->like('spesifikasi.cpu', $data["cpu"]);
		}
		if (!empty($data["brand"])) {
			$this->db->where('spesifikasi.idb', $data["brand"]);
		}
		if (!empty($data["kamera"])) {
			$this->db->where('spesifikasi.main_camera', $data["kamera"]);
		}

		return $this->db->get()->result_array();
	}
}