<?php 
class Data_model extends CI_Model
{
	public $table = "data";

	function all()
	{
		$this->db->select('data.*, spesifikasi.name AS name');
		$this->db->from($this->table);
		$this->db->join('spesifikasi', 'spesifikasi.ids = data.spesifikasi_id');
		
		return $this->db->get()->result_array();
	}

	function insert($data)
	{
		$this->db->insert($this->table, $data);
	}
}