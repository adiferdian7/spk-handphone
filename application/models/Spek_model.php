<?php 


class Spek_model extends CI_Model
{
	public $table = 'spesifikasi';
	public $id    = 'ids';
	public $idb = 'spesifikasi.idb';
	public $ids = 'spesifikasi.ids';
  // public $order = 'DESC';

	public function getALLPonsel_spek($ids)
	{
		$query = $this->db->select('ids, spesifikasi.foto, spesifikasi.name, brand.brand, 
			spesifikasi.harga, spesifikasi.network, spesifikasi.launching, 
			spesifikasi.dimensi, spesifikasi.berat, spesifikasi.bahan, 
			spesifikasi.kartu, spesifikasi.panel, spesifikasi.ukuran, 
			spesifikasi.resolusi, spesifikasi.proteksi, spesifikasi.platform, 
			spesifikasi.cpu, spesifikasi.slot, spesifikasi.kapasitas, spesifikasi.ram,
			spesifikasi.main_camera, spesifikasi.front_camera, spesifikasi.loundspeker, 
			spesifikasi.jack, spesifikasi.wlan, spesifikasi.bluetooth, 
			spesifikasi.gps, spesifikasi.nfc, spesifikasi.infrared, 
			spesifikasi.radio, spesifikasi.usb, spesifikasi.fitur, 
			spesifikasi.tambahan, spesifikasi.battery, spesifikasi.qc, spesifikasi.harga')->where($this->ids,$ids)->join('brand', 'spesifikasi.idb = brand.idb')->order_by($this->id,'DESC')->get($this->table);
		return $query->result_array();
	}

	public function coba($id){
		$data = $this->db->select('spesifikasi.name, brand.brand, jenis.jenis')->where($this->ids,$id)->join('brand', 'spesifikasi.idb = brand.idb')->join('jenis', 'spesifikasi.idj=jenis.idj')->get($this->table);
		return $data->result_array();
	}

	public function getbrand($id)
	{
		$query = $this->db->select('brand.brand')->where($this->id,$id)->join('brand', 'spesifikasi.idb = brand.idb')->order_by($this->id,'DESC')->get($this->table);
		return $query->row_array();
	}

	public function getALL($id)
	{
		$query = $this->db->order_by($this->id,'DESC')->get($this->table);
		return $query->result_array();
	}

	function insertspek($data)
	{
		$this->db->insert($this->table, $data);
		
	}

	public function getSpekid($id)
	{
		$query = $this->db->where($this->id,$id)->get($this->table);
		return $query->row_array();
	}

	function updatespek($data, $id)
	{
		$this->db->where('idbe',$id);
		$this->db->update($this->table, $data);
		
	}

	function updatespekcuk($data, $id){
		$this->db->where('ids',$id);
		$this->db->update($this->table, $data);
	}

	function hapuspek($id){
		$this->db->where('ids',$id);
		$this->db->delete($this->table);
	}


	function updatespekcok($data, $id){
		$this->db->where('spesifikasi_id', $id);
		$this->db->update('data', $data);
	}
}