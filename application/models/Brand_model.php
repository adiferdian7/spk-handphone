<?php 


class Brand_model extends CI_Model
{
	public $table = 'brand';
	public $id    = 'idb';
	public $table2 = 'spesifikasi';
	public $id2 = 'spesifikasi.idb';
  // public $order = 'DESC';

	public function getALLPonsel()
	{
		$query = $this->db->get('brand');
		return $query->result_array();
	}

	function insertb($data)
	{
		$this->db->insert($this->table, $data);
	}

	public function getBrandid($id)
	{
		$query = $this->db->where($this->id,$id)->get($this->table);
		return $query->row_array();
	}

	public function getALLPonsel_all_spek($idb)
	{
		$query = $this->db->select('ids, foto, spesifikasi.name, brand.brand, spesifikasi.harga, spesifikasi.network, spesifikasi.launching, spesifikasi.dimensi, spesifikasi.berat, spesifikasi.bahan, spesifikasi.kartu, spesifikasi.panel, spesifikasi.ukuran, spesifikasi.resolusi, spesifikasi.proteksi, spesifikasi.platform, spesifikasi.cpu, spesifikasi.slot, spesifikasi.kapasitas, spesifikasi.main_camera, spesifikasi.front_camera, spesifikasi.loundspeker, spesifikasi.jack, spesifikasi.wlan, spesifikasi.bluetooth, spesifikasi.gps, spesifikasi.nfc, spesifikasi.infrared, spesifikasi.radio, spesifikasi.usb, spesifikasi.fitur, spesifikasi.tambahan, spesifikasi.battery, spesifikasi.qc, spesifikasi.harga')->where($this->id2,$idb)->join('brand', 'spesifikasi.idb = brand.idb')->order_by($this->id2,'DESC')->get($this->table2);
		return $query->result_array();
	}

	public function hapusbrand($id){
		$this->db->where('idb',$id);
		$this->db->delete($this->table);
	}
}