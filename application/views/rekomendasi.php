
<!-- Start page content -->
<div id="page-content" class="page-wrapper section" style="padding-top: 30px;">

	<!-- LOGIN SECTION START -->
	<div class="login-section mb-80">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-2 mx-auto">
					<div class="my-account-content" id="accordion">
						<!-- My Personal Information -->
						<div class="card mb-15">
							<div  class="card-header" id="headingOne">
								<h2 class="card-title">
									<a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Rekomendasi Smartphone</a>
								</h2>
							</div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <select style="height: 40px;" id="harga" class="form-control">
                                            <option value="1337">Semua harga</option>
                                            <option value="1">0 - 3 Juta</option>
                                            <option value="2">3 - 6 Juta</option>
                                            <option value="3">6 - 10 Juta</option>
                                            <option value="4">> 10 Juta</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6"></div>
                                    <div class="col-sm-4">
                                        <a style="height: 40px; float: right;"   href="<?= base_url("auth/rekomendasi_custom") ?>" class="button pl-3 pr-3 ">Ubah Nilai Rekomendasi</a>
                                    </div>
                                </div>

                                <br />

                                <table class="table" style="color: #000000;" id="table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Merk</th>
                                            <th>CPU</th>
                                            <th>RAM</th>
                                            <th>Baterai</th>
                                            <th>Harga</th>
                                            <th>Nilai</th>
                                        </thead>
                                        <tbody id="data">
                                            <?php
                                            $i = 1;
                                            foreach ($data as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?>.</td>
                                                    <td><a href="<?=base_url('auth/spek/').$value['ids'] ?>" class="btn-hover-2"><?= $value["name"] ?></a></td>
                                                    <td><?= $value['cpu'];  ?></td>
                                                    <td><?= $value['ram'];  ?>Gb</td>
                                                    <td><?= $value['battery'];  ?>Mah</td>
                                                    <td>Rp. <?=  number_format($value['harga'], 0, ",", ".");   ?> Rupiah</td>
                                                    <td><?= $value["value"] ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $("#harga").change(function() {
            $.ajax({
                url         : `<?= base_url("auth/rekomendasi_search") ?>/`+$("#harga").val(),
                dataType    : "json",
                type        : "GET",
                beforeSend: function (e) {
                    $("#data").empty();
                    $("#data").append(`
                        <tr>
                        <td colspan="3">Mohon tunggu...</td>
                        </tr>
                        `);
                },
                success: function(response) {
                    $("#data").empty();
                    if (response.length > 0) {
                        response.map((data, key) => {
                            $("#data").append(`
                                <tr>
                                <td>${ key+1 }.</td>
                                <td>${ data.name }</td>
                                <td>${ data.cpu }</td>
                                <td>${ data.ram }Gb</td>
                                <td>${ data.battery }Mah</td>
                                <td>Rp.${ data.harga }</td>
                                </tr>
                                `); 
                        });
                    }
                    else {
                        $("#data").append(`
                            <tr>
                            <td colspan="3">Hasil tidak ditemukan.</td>
                            </tr>
                            `);
                    }
                },
                error: function (jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        information = "Not connect (verify network).";
                    } else if (jqXHR.status == 404) {
                        information = "Requested page not found.";
                    } else if (jqXHR.status == 500) {
                        information = "Internal Server Error.";
                    } else if (exception === "parsererror") {
                        information = "Requested JSON parse failed.";
                    } else if (exception === "timeout") {
                        information = "Time out error.";
                    } else if (exception === "abort") {
                        information = "Ajax request aborted.";
                    } else {
                        information = "Uncaught Error ("+jqXHR.responseText+").";
                    }
                    console.log(information);
                }
            });
        });


        $("#table").DataTable({
            "paging":   true,
            //"iDisplayLength": 8,
            "bFilter": false,
        });
    </script>