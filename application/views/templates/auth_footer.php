        <!-- START FOOTER AREA -->
        <footer id="footer" class="footer-area section">
            <div class="footer-top">
                <div class="container-fluid">
                    <div class="plr-185">
                        <div class="footer-top-inner gray-bg">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="single-footer footer-about">
                                        <div class="footer-brief">
                                            <p>Ini ceritanya adalah website pendukung keputusan pembelian smartphone menggunakan metode AHP (Analitikal Hirarki Proccess) yang ditujukan untuk memenuhi Tugas Akhir saya sebagai mahasiswa TI (Teknik Informatika).
                                            </p>
                                            <p>Banyak sekali pihak yang membantu saya dalam mengerjakan Tugas Akhir saya baik bantuan dalam segi materiil maupun moril dan bahkan ada dalam bentuk yang aneh-aneh yang tidak bisa saya jelaskan dengan kata-kata</p>
                                        </div>
                                        <ul class="footer-social mx-auto">
                                            <li>
                                                <a class="facebook" href="#" title="Facebook"><i
                                                        class="zmdi zmdi-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a class="instagram" href="#" title="Google Plus"><i
                                                        class="zmdi zmdi-instagram"></i></a>
                                            </li>
                                            <li>
                                                <a class="twitter" href="#" title="Twitter"><i class="zmdi zmdi-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a class="whatsapp" href="#" title="RSS"><i class="zmdi zmdi-whatsapp"></i></a>
                                            </li>
                                            <li>
                                                <a class="rss" href="#" title="RSS"><i class="zmdi zmdi-rss"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER AREA -->
    <!-- Popper js -->
    <script src="<?= base_url('assets/') ?>js/popper.min.js"></script>
    <!-- jquery.nivo.slider js -->
    <script src="<?= base_url('assets/') ?>lib/js/jquery.nivo.slider.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</body>
</html>