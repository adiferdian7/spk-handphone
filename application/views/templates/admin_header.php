<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?= $title ?>
    </title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="apple-touch-icon" href="<?= base_url('assets/') ?>https://i.imgur.com/QRAUqs9.png"> -->
    <!-- <link rel="shortcut icon" href="<?= base_url('assets/') ?>https://i.imgur.com/QRAUqs9.png"> -->

    <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>awesome/css/fontawesome.css" type='text/css'> -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>awesome/css/fontawesome.min.css" type='text/css'> -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>css/normalize.min.css"> -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>css/themify-icons.css"> -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>css/pe-icon-7-stroke.min.css"> -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/') ?>css/flag-icon.min.css"> -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/style.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <!-- <link href="<?= base_url('assets/') ?>css/chartist.min.css" rel="stylesheet"> -->
    <!-- <link href="<?= base_url('assets/') ?>css/jqvmap.min.css" rel="stylesheet"> -->

    <!-- <link href="<?= base_url('assets/') ?>css/weather-icons.css" rel="stylesheet" /> -->
    <!-- <link href="<?= base_url('assets/') ?>css/fullcalendar.min.css" rel="stylesheet" /> -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <style>
        #weatherWidget .currentDesc {
            color: #ffffff!important;
        }
        .traffic-chart {
            min-height: 335px;
        }
        #flotPie1  {
            height: 150px;
        }
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
           height: 105px;
       }

       #flotBarChart {
        height: 150px;
    }
    #cellPaiChart{
        height: 160px;
    }


    .navbar-nav {
        float: none;
        position: relative;
        >li { 
            padding-left: 30px;
            padding-right:30px;
            &.active {
                background: $menu-active-bg;
            }
        }
        li {
            width: 100%;
            &.active .menu-icon,
            &:hover .toggle_nav_button:before,
            .toggle_nav_button.nav-open:before {
                color: $menu-active;
            }
            .dropdown-toggle:after {
                display: none;
            }
            >a {
                background: none !important;
                color: $menu-color;
                display: inline-block; 
                font-size: 14px;
                line-height: 26px;
                padding: 10px 0;
                position: relative;
                width: 100%;
                &:hover,
                &:hover .menu-icon {
                    color: $menu-active ;
                }
                .menu-icon {
                    color: $menu-color;
                    float: left;
                    margin-top: 8px;
                    width: 55px;
                    text-align: left;
                    z-index: 9;
                }
                .menu-title-text {
                    font-size: 14px;
                }
                .badge {
                    border-radius: 0; 
                    font-weight: 600;
                    float: right;
                    margin: 6px 0 0 0;
                    padding: 0.4em 0.5em;
                }
            }
        }
    }

</style>
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu"><h6>Administrator</h6>
                        <hr class="sidebar-divider my-0">
                    </li><!-- /.menu-title -->
                    <li>
                        <a href="<?= base_url('auth_admin/aindex')  ?>">
                            <i class="menu-icon fa fa-home" style="padding-left: 20px">
                            </i>Beranda </a>
                        </li>
                        <li class="menu"><h6>Akun</h6>
                            <hr class="sidebar-divider my-0">
                        </li><!-- /.menu-title -->
                        <li>
                            <a href="<?= base_url('auth_admin/admin')  ?>"> 
                                <i class="menu-icon fa fa-smile-o" style="padding-left: 20px">
                                </i>Admin</a>
                            </li>
                                <li class="menu"><h6>Perhitungan</h6>
                                    <hr class="sidebar-divider my-0">
                                </li><!-- /.menu-title -->
                                <li>
                                    <a href="<?= base_url('auth_admin/penilaian')  ?>">
                                        <i class="menu-icon fa fa-eye" style="padding-left: 20px">
                                        </i>Penilaian</a>
                                        <li>
                                            <a href="<?= base_url('auth_admin/penilaian_hasil')  ?>"> 
                                                <i class="menu-icon fa fa-tablet" style="padding-left: 20px">
                                                </i>Hasil Penilaian</a>
                                            </li>
                                            </li>
                                            <li class="menu"><h6>Konten</h6>
                                                <hr class="sidebar-divider my-0">
                                            </li><!-- /.menu-title -->
                                            <li>
                                                <a href="<?= base_url('auth_admin/berita')  ?>">
                                                    <i class="menu-icon fa fa-eye" style="padding-left: 20px">
                                                    </i>Berita</a>
                                                    <li>
                                                        <a href="<?= base_url('auth_admin/brand')  ?>"> 
                                                            <i class="menu-icon fa fa-tablet" style="padding-left: 20px">
                                                            </i>Brand</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= base_url('auth_admin/spek')  ?>"> 
                                                                <i class="menu-icon fa fa-tablet" style="padding-left: 20px">
                                                                </i>Ponsel</a>
                                                            </li>
                                                        </li>
                                                        <li>
                                                            <li class="menu"><h6>Upload</h6>
                                                                <hr class="sidebar-divider my-0">
                                                            </li><!-- /.menu-title -->
                                                            <li>
                                                                <li>
                                                                    <a href="<?= base_url('auth_admin/inb')  ?>"> 
                                                                        <i class="menu-icon fa fa-quote-left" style="padding-left: 20px">
                                                                        </i>Berita
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= base_url('auth_admin/ins')  ?>"> 
                                                                        <i class="menu-icon fa fa-mobile" style="padding-left: 20px">
                                                                        </i>Spesifikasi Ponsel
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.navbar-collapse -->
                                                    </nav>
                                                </aside>
                                                <!-- /#left-panel -->

                                                <!-- Right Panel -->
                                                <div id="right-panel" class="right-panel">
                                                    <!-- Header-->
                                                    <header id="header" class="header">
                                                        <div class="top-left">
                                                            <div class="navbar-header">
                                                                <a class="navbar-brand" href="<?= base_url('auth_admin/aindex')  ?>"><img src="<?= base_url('assets/') ?>images/logo.png" alt="Logo"></a>
                                                                <a class="navbar-brand hidden" href="<?= base_url('auth_admin/aindex')  ?>"><img src="<?= base_url('assets/') ?>images/logo2.png" alt="Logo"></a>
                                                            </div>
                                                        </div>
                                                        <div class="top-right">
                                                            <div class="header-menu">

                                                                <div class="user-area dropdown float-right">
                                                                    <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        <img class="user-avatar rounded-circle" src="<?= base_url('assets/')  ?>images/admin.png" alt="User Avatar">
                                                                    </a>
                                                                    <div class="user-menu dropdown-menu">
                                                                        <a class="nav-link" href="#"><i ></i>Profile</a>
                                                                        <a class="nav-link" href="<?= base_url('auth/logout')  ?>"><i></i>Logout</a>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </header>
        <!-- /#header -->