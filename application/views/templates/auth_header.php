<!DOCTYPE html>
<html class="no-js" lang="en">


<!-- Mirrored from demo.hasthemes.com/subas-preview-v1/subas/index-10.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 06 Dec 2019 05:26:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= $title;  ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/') ?>img/icon/favicon.png">

    <!-- All CSS Files -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/bootstrap.min.css">
    <!-- Nivo-slider css -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>lib/css/nivo-slider.css">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/responsive.css">
    <!-- User style -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/custom.css">
    
    <!-- Style customizer (Remove these two lines please) -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/style-customizer.css">

    <!-- Modernizr JS -->
    <script src="<?= base_url('assets/') ?>js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body class="wide-layout">
    <!-- Body main wrapper start -->
    <div class="wrapper">

        <!-- START HEADER AREA -->
        <header class="header-area header-wrapper header-2">
            <!-- header-middle-area -->
            <div id="sticky-header" class="header-middle-area plr-185">
                <div class="container-fluid">
                    <div class="full-width-mega-dropdown">
                        <div class="row">
                            <!-- logo -->
                            <div class="col-lg-2 col-md-4">
                                <div class="logo">
                                    <a href="<?= base_url('auth/index')?>">
                                        <img src="<?= base_url('assets/') ?>img/logo/logo.png" alt="main logo">
                                    </a>
                                </div>
                            </div>
                            <!-- primary-menu -->
                            <div class="col-lg-8 d-none d-lg-block">
                                <nav id="primary-menu">
                                    <ul class="main-menu text-center">
                                        <li><a href="<?= base_url('auth/index')  ?>">Beranda</a>
                                        </li>
                                        <li><a href="<?= base_url('auth/berita')  ?>">Berita</a>
                                        </li>
                                        <li><a href="<?= base_url('auth/brand')  ?>">Brand</a>
                                        </li>                                        
                                        <li><a href="<?= base_url('auth/pencarian')  ?>">Pencarian Smartphone</a>
                                        </li>
                                        <li><a href="<?= base_url('auth/rekomendasi')  ?>">Rekomendasi</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- header-search & total-cart -->
                            <div class="col-lg-2 col-md-8">
                                <div class="search-top-cart  f-right">
                                    <!-- header-search -->
                                    <div class="header-search header-search-2 f-left">
                                        <div class="header-search-inner">
                                            <button class="search-toggle">
                                                <i class="zmdi zmdi-search"></i>
                                            </button>
                                            <form action="<?= base_url('auth/cari') ?>" method="post">
                                                <div class="top-search-box">
                                                    <input type="text" name="name" placeholder="Cari Nama Ponsel...">
                                                    <button type="submit">
                                                        <i class="zmdi zmdi-search"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- total-cart -->
                                    <?php if($this->session->userdata('is_login')): ?>
                                        <div class="total-cart f-left">
                                            <div class="total-cart-in">
                                                <div class="cart-toggler">
                                                    <a href="<?=base_url().'auth_admin/aindex'?>">
                                                        <span class="cart-quantity"></span><br>
                                                        <span class="cart-icon">
                                                            Admin
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                    <div class="total-cart f-left">
                                        <div class="total-cart-in">
                                            <div class="cart-toggler">
                                                <a href="#">
                                                    <span class="cart-quantity"></span><br>
                                                    <span class="cart-icon">
                                                        LOGIN
                                                    </span>
                                                </a>
                                            </div>
                                            <ul>
                                                <form method="post" action="<?= base_url('auth/check_login')  ?>">
                                                    <li>
                                                        <div class="top-cart-inner subtotal">
                                                            <div>
                                                                <input name="username" type="text" placeholder="Username">
                                                                <input name="password" type="Password" placeholder="Password">
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="top-cart-inner view-cart">
                                                            <button class="submit-btn-1 mt-20 btn-hover-1" type="submit"
                                                            value="register">Login</button>
                                                            </div>
                                                        </li>
                                                    </form>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- END HEADER AREA -->

                <!-- START MOBILE MENU AREA -->
                <div class="mobile-menu-area d-block d-lg-none section">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="mobile-menu">
                                    <nav id="dropdown">
                                        <ul>
                                            <li><a href="<?=base_url()?>">Beranda</a>
                                            </li>
                                            <li><a href="<?=base_url()?>/views/berita.php">Berita</a>
                                            </li>
                                            <li><a href="shop.html">Review</a>
                                            </li>
                                            <li><a href="#">Brand</a>
                                            </li>                                                                   
                                            <li><a href="about.html">About us</a>
                                            </li>
                                            <li><a href="contact.html">Contact</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MOBILE MENU AREA -->

                <!-- START SLIDER AREA -->
                <div class="slider-area slider-2 section">
                    <div class="bend niceties preview-2">
                        <div id="nivoslider-2" class="slides">
                            <img src="<?= base_url('assets/') ?>img/slider/slider-2/slider-1.jpg" style="height: 110px; width: 100%;" alt="" title="#slider-direction-1" />
                        </div>
                    </div>
                </div>
                <!-- END SLIDER AREA -->
