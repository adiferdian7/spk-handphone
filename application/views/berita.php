
<!-- START PAGE CONTENT -->
<section id="page-content" class="page-wrapper section">

    <!-- PRODUCT TAB SECTION START -->
    <div class="product-tab-section pt-80 pb-55">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title text-left mb-40">
                        <h2 class="uppercase">Berita Terbaru</h2>
                        <h6>Berita terbaru mengenai update dunia gadget.</h6>
                    </div>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- UP COMMING PRODUCT SECTION START -->
                <?php foreach ($vberita as $key => $value): ?>
                    <div class="up-comming-product-section ptb-20">
                        <div class="container">
                            <div class="row">
                                <!-- up-comming-pro -->
                                <div class="col-lg-12">
                                    <div class="up-comming-pro1 gray-bg up-comming-pro-2 clearfix">
                                        <div class="up-comming-pro-img1 f-left">
                                            <a href="<?php echo base_url('auth/vberita?id=').$value['idbe'] ?>">
                                                <img src="<?= base_url('assets/gambar/berita/').$value['foto']?>" style="height: 340px; width: 330px;" alt="">
                                            </a>
                                        </div>

                                        <div class="up-comming-pro-info f-left">
                                            <a href="<?php echo base_url('auth/vberita?id=').$value['idbe'] ?>"><h3><?= $value['judul'] ?></h3></a>
                                            <p><?php echo substr($value['isi'], 0, 300)  ?>
                                            ....
                                            <a href="<?php echo base_url('auth/vberita?id=').$value['idbe'] ?>">Lihat Selengkapnya</a>
                                        </p>
                                        <div class="up-comming-time-2 clearfix">
                                            <div data-countdown="2019/01/15"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            <!-- UP COMMING PRODUCT SECTION END -->
        </div>
    </div>
</div>

<!-- product-item end -->
    <!-- Body main wrapper end -->