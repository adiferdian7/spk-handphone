

<!-- Start page content -->
<section style="padding-top: 90px" id="page-content" class="page-wrapper section">

    <!-- SHOP SECTION START -->
    <div class="shop-section mb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- single-product-area start -->
                    <?php foreach ($spesifikasi as $key => $value): ?>
                        <div class="single-product-area mb-80">
                            <div class="row">
                                <!-- imgs-zoom-area start -->
                                <div class="col-lg-5">
                                    <div class="imgs-zoom-area">
                                        <img src="<?= base_url().$value['foto'] ?>" alt="">
                                    </div>
                                </div>
                                <!-- imgs-zoom-area end -->
                                <!-- single-product-info start -->
                                
                                <div class="col-lg-7" style="margin-top: 30px;"> 
                                    <div class="single-product-info">
                                        <h1 class="text-black-1"><?= $value['name'];  ?></h1>
                                        <h4><?= $value['brand'];  ?></h4>
                                        <!--  hr -->
                                        <hr>
                                        <!-- single-product-price -->
                                        <p>
                                            <h2 class="pro-price">
                                                Harga
                                            </h2>
                                        </p>
                                        <h3>Rp. <?=  number_format($value['harga'], 0, ",", ".");   ?> Rupiah</h3>

                                        <!-- single-product-tab -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <!-- hr -->
                                                <hr>
                                                <div class="single-product-tab reviews-tab">
                                                    <ul class="nav mb-20">
                                                        <li><h3><a class="active" href="" data-toggle="tab">Spesifikasi <?=  $value['brand']; ?><?php echo(' ') ?><?=  $value['name']; ?></a></h3>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active show" id="description">
                                                                <table>
                                                                    <tr><td>
                                                                        <h4>Jaringan</h4>
                                                                    </td>
                                                                    <td>
                                                                        Teknologi
                                                                    </td>
                                                                    <td>
                                                                        <?= join(", ", json_decode($value['network'], true));  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                        <h4>
                                                                            launching
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Tahun
                                                                    </td><td>
                                                                        <?= $value['launching'];  ?>
                                                                    </td>
                                                                </tr>
                                                                <!-- body -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Body
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Dimensi
                                                                    </td><td>
                                                                        <?= $value['dimensi'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Berat
                                                                    </td><td>
                                                                        <?= $value['berat'];  ?> gram
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Bahan
                                                                    </td><td>
                                                                        <?= $value['bahan'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Kartu
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['kartu'];  ?>
                                                                    </td>
                                                                </tr>
                                                                <!-- Display -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Display
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Panel
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['panel'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Ukuran Diagonal
                                                                    </td><td>
                                                                        <?= $value['ukuran'];  ?> Inch
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Resolusi Layar
                                                                    </td><td>
                                                                        <?= $value['resolusi'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Proteksi Layar
                                                                    </td><td>
                                                                        <?= $value['proteksi'];  ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Platform -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Platform
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        OS
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['platform'];  ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- CPU -->
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        CPU
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['cpu'];  ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Memory -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Memory
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Slot Memory
                                                                    </td>
                                                                    <td>
                                                                        <?php 
                                                                        if ( $value['slot'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>

                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    <td>
                                                                        Kapasitas Memory
                                                                    </td></td>
                                                                    <td>
                                                                        <?= $value['kapasitas'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    <td>
                                                                        Kapasitas RAM
                                                                    </td></td>
                                                                    <td>
                                                                        <?= $value['ram'];  ?> Gb
                                                                    </td>
                                                                </tr>

                                                                <!-- Camera -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Kamera
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Kamera Belakang
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['main_camera'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    <td>
                                                                        Kamera Depan
                                                                    </td></td>
                                                                    <td>
                                                                        <?= $value['front_camera'];  ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Speker -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Audio
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Loundspeker
                                                                    </td>
                                                                    <td>
                                                                        <?php 
                                                                        if ( $value['loundspeker'] == 0) {
                                                                            echo('Mono');
                                                                        } else {
                                                                            echo('Stereo');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    <td>
                                                                        Slot Jack Audio 3.5mm
                                                                    </td></td>
                                                                    <td>
                                                                        <?php 
                                                                        if ( $value['jack'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Fitur -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Fitur
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        WLAN
                                                                    </td>
                                                                    <td>
                                                                        <?php 
                                                                        if ( $value['wlan'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Bluetooth
                                                                    </td><td>
                                                                        <?php 
                                                                        if ( $value['bluetooth'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Integrated GPS
                                                                    </td><td>
                                                                        <?php 
                                                                        if ( $value['gps'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        NFC
                                                                    </td><td>
                                                                        <?php 
                                                                        if ( $value['nfc'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Infrared Blaster
                                                                    </td><td>
                                                                        <?php 
                                                                        if ( $value['infrared'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>

                                                                    </td>
                                                                    <td>
                                                                        Radio
                                                                    </td><td>
                                                                        <?php 
                                                                        if ( $value['radio'] == 0) {
                                                                            echo('Tidak Ada');
                                                                        } else {
                                                                            echo('Ada');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Usb -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            USB
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Port USB
                                                                    </td>
                                                                    <td>
                                                                        <?php 
                                                                        if ( $value['usb'] == 0) {
                                                                            echo('Micro B');
                                                                        } else if ( $value['usb'] == 1) {
                                                                            echo('Micro C');
                                                                        } else {
                                                                            echo('Lightning');
                                                                        }

                                                                        ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Sensor -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Sensor
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Sensor
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['fitur'];  ?>
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Fitur Tambahan
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['tambahan'];  ?>
                                                                    </td>
                                                                </tr>

                                                                <!-- Baterai -->
                                                                <tr>
                                                                    <td>
                                                                        <h4>
                                                                            Baterai
                                                                        </h4>
                                                                    </td>
                                                                    <td>
                                                                        Kapasitas Baterai
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['battery'];  ?> Mah
                                                                    </td>
                                                                </tr><tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Teknologi Charging
                                                                    </td>
                                                                    <td>
                                                                        <?= $value['qc'];  ?> Fast Charging
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--  hr -->
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                    <!-- single-product-info end -->
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->             

    </section>
        <!-- End page content -->