

<!-- Start page content -->
<div id="page-content" class="page-wrapper section">

    <!-- PRODUCT TAB SECTION START -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title text-left mb-40">
                    <h2 class="uppercase" style="padding-top: 30px;"><?= $brand['brand'];  ?></h2>
                    <h6>Daftar dari list produk brand <?= $brand['brand'];  ?></h6>
                </div>
            </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- popular-product start -->
            <div id="popular-product" class="tab-pane active show">
                <div class="row">
                    <!-- product-item start -->
                    <?php foreach ($spesifikasi as $key => $value): ?>
                        <div class="col-lg-3 col-md-4">
                            <div class="product-item">
                                <div class="product-img">
                                    <a href="<?= base_url('auth/spek/').$value['ids'] ?>">
                                        <img  src="<?= base_url().$value['foto'] ?>" alt="" />
                                    </a>
                                </div>
                                <div class="product-info">
                                        <center><h3 class="pro-price"><a href="<?=base_url('auth/spek/').$value['ids'] ?>" class="btn-hover-2"><?= $value['name'];  ?></a></h3></center>
                                    <h4>Rp. <?=  number_format($value['harga'], 0, ",", ".");   ?> Rupiah</h4>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    <!-- product-item end -->
                </tr>
            </div>
        </div>
        <!-- leave your comment -->
    </div>
</div>