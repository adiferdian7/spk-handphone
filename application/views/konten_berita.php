
<!-- BLOG SECTION START -->
<center><div class="blog-section mb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="blog-details-area">
                    <!-- blog-details-title -->
                    <h2 class="blog-details-title mb-30"  style="padding-top: 30px"><?= $berita['judul'] ?></h2>
                    <!-- blog-description -->

                    <!-- blog-details-photo -->
                    <div class="blog-details-photo bg-img-1 p-20 mb-30">
                        <img style="max-height: 300px; max-width: 300px;" src="<?= base_url('assets/gambar/berita/').$berita['foto'] ?>" alt="">
                        <div class="today-date bg-img-1">
                            <span class="meta-date">30</span>
                            <span class="meta-month">June</span>
                        </div>
                    </div>
                    <div>
                        <h3><p><?php echo $berita['isi']  ?></p></h3>
                    </div>
                    

                    <div class="leave-comment" style="padding-top: 150px;">
                        <h4 class="blog-section-title border-left mb-30">leave your comment</h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" name="name" placeholder="Nama...">
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="email" placeholder="Alamat E-mail...">
                            </div>
                            <div class="col-lg-12">
                                <textarea class="custom-textarea" placeholder="Komentar Anda..."></textarea>
                            </div>
                        </div>
                        <button class="submit-btn-1 black-bg mt-30 btn-hover-2" type="submit">Komentar</button>
                    </div>
                    <!--  -->
                    <!-- comments on t this post -->
                    <div style="padding-top: 60px" class="post-comments mb-60">
                        <h4 class="blog-section-title border-left mb-30">comments on this product</h4>
                        <!-- single-comments -->
                        <div class="media mt-30">
                            <div class="media-left pr-30">
                                <a href="#"><img class="media-object" src="<?= base_url('assets/') ?>img/author/2.jpg" alt="#"></a>
                            </div>
                            <div class="media-body">
                                <div class="clearfix">
                                    <div class="name-commenter f-left">
                                        <h6 class="media-heading"><a href="#">Gerald Barnes</a></h6>
                                        <p class="mb-10">27 Jun, 2019 at 2:30pm</p>
                                    </div>
                                    <ul class="reply-delate f-right">
                                        <li><a href="#">Reply</a></li>
                                        <li>/</li>
                                        <li><a href="#">Delate</a></li>
                                    </ul>
                                </div>
                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas elese ifend. Phasellus a felis atestese bibendum feugiat ut eget eni Praesent  messages in con sectetur posuere dolor non.</p>
                            </div>
                        </div>
                    </div>
                    <!-- leave your comment -->
                </div>
            </div>
        </div>
    </div>
</div>
</center>
<!-- BLOG SECTION END -->      