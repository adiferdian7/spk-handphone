
<!-- START PAGE CONTENT -->
<section id="page-content" class="page-wrapper section">

    <!-- UP COMMING PRODUCT SECTION START -->
    <div style="padding-bottom: 100px;" class="up-comming-product-section ptb-20">
        <div class="container">
            <div class="row">
                <!-- up-comming-pro -->
                <div class="col-lg-8">
                    <div class="up-comming-pro gray-bg up-comming-pro-2 clearfix">
                        <div class="up-comming-pro-img f-left">
                            <a href="<?php echo base_url('auth/vberita?id=').$berita['idbe'] ?>">
                                <img src="<?= base_url('assets/gambar/berita/').$berita['foto'] ?>" style="height: 340px; width: 330px;" alt="">
                            </a>
                        </div>
                        <div class="up-comming-pro-info f-left">
                            <h3><a href="<?php echo base_url('auth/vberita?id=').$berita['idbe'] ?>"><?= $berita['judul'] ?></a></h3>
                            <p><?php echo substr($berita['isi'], 0, 300)  ?>
                            ....
                            <a href="<?php echo base_url('auth/vberita?id=').$berita['idbe'] ?>">Lihat Selengkapnya</a>
                        </p>
                        <div class="up-comming-time-2 clearfix">
                            <div data-countdown="2019/01/15"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-block d-md-none d-lg-block">
                <div class="banner-img">
                    <img src="<?= base_url('assets/') ?>img/banner/1.jpg" alt="">
                </div>
                <div class="banner-info1">
                    <nav id="primary-menu" class="banner-info col-lg-4">
                        <table style="width: 300px; height: 250px;">
                            <tr>
                                <td colspan="4"><center>
                                    <h3 class="banner-title-2 btn-hover-2"><a href="<?= base_url('auth/pencarian')  ?>">Pencarian Spesifik</a></h3></td>
                                </center></tr>
                                <?php 

                                foreach ($brand as $key => $value){
                                    if($key%4 == 0){
                                        ?>
                                        <tr>
                                            <td>
                                                <center><h4><a href="<?=base_url('auth/list/').$value['idb'] ?>" class="btn-hover-2"><?=$value['brand']?></a></h4></center>
                                            </td>
                                            <?php
                                        }else{
                                            ?>
                                            <td>
                                                <center><h4><a href="<?=base_url('auth/list/').$value['idb'] ?>" class="btn-hover-2"><?=$value['brand']?></a></h4></center>
                                            </td>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- UP COMMING PRODUCT SECTION END -->

<!-- BANNER-SECTION START -->
<div class="banner-section ptb-60">
    <div class="container">
        <div class="row">
            <!-- banner-item start -->
            <?php foreach ($spesifikasi as $key => $value): ?>
                
                <div class="col-lg-4 col-md-6">
                    <div>
                        <div class="banner-img">
                            <a href="<?=base_url('auth/spek/').$value['ids'] ?>"><img style="width: 360px; max-height: 360px;" src="<?= base_url().$value['foto'] ?>" alt=""></a>
                        </div>
                        <h3 style="padding-top: 20px; padding-bottom: 80px;" class="banner-title-2">
                            <center><a href="<?=base_url('auth/spek/').$value['ids'] ?>"><?= $value['name'];  ?></a>
                            </center></h3>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    <!-- BANNER-SECTION END -->
</div>
    <!-- Body main wrapper end -->