
<!-- Start page content -->
<div id="page-content" class="page-wrapper section" style="padding-top: 30px;">
    <!-- LOGIN SECTION START -->
    <div class="login-section mb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="my-account-content" id="accordion">
                        <!-- My Personal Information -->
                        <div class="card mb-15">
                            <div  class="card-header" id="headingOne">
                                <h2 class="card-title">
                                    <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Pencarian Smartphone</a>
                                </h2>
                            </div>
                            <div class="card-body card-block">
                                <form action="<?= base_url('auth/search_manual')  ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="multiple-select" class=" form-control-label">Brand</label>
                                        </div>
                                        <div class="col col-md-9">
                                            <select name="brand" id="multiple-select" multiple="" class="form-control">
                                                <?php foreach ($brand as $br): ?>
                                                    <option style="padding-top: 5px" value="<?= $br['idb']; ?>">
                                                        <?= $br['brand']; ?>
                                                    </option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="email-input" class=" form-control-label">Platform</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <div class="form-check-inline form-check">
                                                <label for="inline-radio3" class="form-check-label ">
                                                    <input type="radio" id="inline-radio3" name="cpu" value="snapdragon" class="form-check-input cpu">Snapdragon &nbsp; &nbsp; &nbsp; &nbsp;
                                                </label>
                                                <label for="inline-radio4" class="form-check-label ">
                                                    <input type="radio" id="inline-radio4" name="cpu" value="mediatek" class="form-check-input cpu">Mediatek &nbsp; &nbsp; &nbsp; &nbsp;
                                                </label>
                                                <label for="inline-radio5" class="form-check-label ">
                                                    <input type="radio" id="inline-radio5" name="cpu" value="kirin" class="form-check-input cpu">Kirin &nbsp; &nbsp; &nbsp; &nbsp;
                                                </label>
                                                <label for="inline-radio6" class="form-check-label ">
                                                    <input type="radio" id="inline-radio6" name="cpu" value="exinos" class="form-check-input cpu">Exinos &nbsp; &nbsp; &nbsp; &nbsp;
                                                </label>
                                                <input type="radio" id="inline-radio6" name="cpu" value="apple" class="form-check-input cpu">Apple
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="email-input" class=" form-control-label">Memory</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="memory" id="selectSm" class="custom-select">
                                            <option value="">Semua Kapasitas RAM.</option>
                                            <option value="3">3 GB</option>
                                            <option value="4">4 GB</option>
                                            <option value="6">6 GB</option>
                                            <option value="8">8 GB</option>
                                            <option value="12">12 GB</option>
                                        </select>
                                    </div>
                                </div>
<!-- 
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="email-input" class=" form-control-label">Jumlah Kamera Utama</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <select name="kamera" id="selectSm" class="custom-select">
                                            <option value="">Semua Jumlah Kamera.</option>
                                            <option value="Single Camera">Single Camera</option>
                                            <option value="Dual Camera">Dual Camera</option>
                                            <option value="Triple Camera">Triple Camera</option>
                                            <option value="Quad Camera">Quad Camera</option>
                                            <option value="Penta Camera">Penta Camera</option>
                                            <option value="Hexa Camera">Hexa Camera</option>
                                        </select>
                                    </div>
                                </div> -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="email-input" class=" form-control-label">Baterai</label>
                                    </div>
                                    <div class="col-9">
                                        <select name="battery" id="selectSm" class="custom-select">
                                            <option value="">Semua ukuran baterai.</option>
                                            <option value="1">2000 - 3000Mah</option>
                                            <option value="2">3000 - 4000Mah</option>
                                            <option value="3">4000 - 5000Mah</option>
                                            <option value="4">Diatas 5000Mah</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="email-input" class=" form-control-label">Harga</label></div>
                                    <div class="col-12 col-md-9">
                                        <select id="harga" name="price" class="custom-select">
                                            <option value="">Semua harga.</option>
                                            <option value="1">0 - 3 Juta</option>
                                            <option value="2">3 - 6 Juta</option>
                                            <option value="3">6 - 10 Juta</option>
                                            <option value="4">> 10 Juta</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="email-input" class=" form-control-label"></label></div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <?= $captcha ?>
                                        </div>
                                        <div class="col-md-12 pt-3">
                                            <input type="text" name="captcha" placeholder="Captcha here." value=""/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div style="padding-left: 250px" class="col-md-6">
                                        <button style="width: 100px" class="submit-btn-1 mt-20 btn-hover-1" type="submit"
                                        name="submit">Cari</button>
                                    </div>
                                    <div style="padding-right: 250px" class="col-md-6">
                                        <button style="width: 100px" class="submit-btn-1 mt-20 btn-hover-1 f-right"
                                        type="reset">Clear</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
