
<!-- Start page content -->
<div id="page-content" class="page-wrapper section" style="padding-top: 30px;">

	<!-- LOGIN SECTION START -->
	<div class="login-section mb-80">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					<div class="my-account-content" id="accordion">
						<!-- My Personal Information -->
						<div class="card mb-15">
							<div  class="card-header" id="headingOne">
								<h2 class="card-title">
									<a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Rekomendasi Smartphone</a>
								</h2>
							</div>
                            <div class="card-body card-block">
                                <table class="table" style="color: #000000;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Merk</th>
                                            <th>CPU</th>
                                            <th>RAM</th>
                                            <th>Baterai</th>
                                            <th>Harga</th>
                                        </thead>
                                        <tbody id="data">
                                            <?php
                                                $i = 1;
                                                foreach ($data as $key => $value) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $i++ ?>.</td>
                                                        <td><a href="<?=base_url('auth/spek/').$value['ids'] ?>" class="btn-hover-2"><?= $value["name"] ?></a></td>
                                                        <td><?= $value['cpu'];  ?></td>
                                                        <td><?= $value['ram'];  ?>Gb</td>
                                                        <td><?= $value['battery'];  ?>Mah</td>
                                                        <td>Rp. <?=  number_format($value['harga'], 0, ",", ".");   ?> Rupiah</td>
                                                    </tr>
                                                    <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>