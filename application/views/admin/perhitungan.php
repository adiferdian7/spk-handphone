<div class="card mb-3">

  <div class="card-header">
    <div class="container">
      <div class="row">
        <div class="col-auto mr-auto">	<h3>Form Data Hasil Kriteria</h3></div>
      </div>
    </div>
  </div>

  <div class="card-body">
    <div class="table-responsive">
      <div class="panel-body">
        <p class="font-weight-bold">Martik Perbandingan Berpasangan</p>
        <table id="example" class="table table-bordered table-hover display">
          <thead class="thead-dark">
            <tr>
              <th><center>Kriteria</center></th>
              <th><center>CPU</center></th>
              <th><center>Memory</center></th>
              <th><center>Battery</center></th>
              <th><center>Price</center></th>
            </tr>
          </thead>

          <tbody>
            <?php
            foreach ($penilaian as $k_penilaian => $v_penilaian) {
              ?>
              <tr>
                <td><?= $v_penilaian["kriteria"]; ?></td>
                <td><?= $v_penilaian["cpu"]; ?></td>
                <td><?= $v_penilaian["memory"]; ?></td>
                <td><?= $v_penilaian["battery"]; ?></td>
                <td><?= $v_penilaian["price"]; ?></td>
              </tr>
              <?php
              @$j_cpu     += $v_penilaian["cpu"];
              @$j_memory  += $v_penilaian["memory"];
              @$j_battery += $v_penilaian["battery"];
              @$j_price   += $v_penilaian["price"];
            }
            ?>

            <tr>
              <td class="table-primary">Jumlah</td>
              <td class="table-primary"><?= $j_cpu ?></td>
              <td class="table-primary"><?= $j_memory ?></td>
              <td class="table-primary"><?= $j_battery ?></td>
              <td class="table-primary"><?= $j_price ?></td>
            </tr>
          </tbody>
        </table>
        <br>
        <p class="font-weight-bold">Nilai Eigen Kriteria</p>

        <table class="table table-bordered table-hover display">
          <thead class="thead-dark">
            <tr>
              <th><center>Kriteria</center></th>
              <th><center>CPU</center></th>
              <th><center>Memory</center></th>
              <th><center>Battery</center></th>
              <th><center>Price</center></th>
              <th><center>Jumlah</center></th>
              <th><center>Rata-rata</center></th>
            </tr>
          </thead>



          <tbody>
            <?php
            foreach ($penilaian as $k_penilaian => $v_penilaian) {
              ?>
              <tr>
                <td><?= $v_penilaian["kriteria"]; ?></td>
                <td><?= $e_cpu      = $v_penilaian["cpu"]/$j_cpu; ?></td>
                <td><?= $e_memory   = $v_penilaian["memory"]/$j_memory; ?></td>
                <td><?= $e_battery  = $v_penilaian["battery"]/$j_battery; ?></td>
                <td><?= $e_price    = $v_penilaian["price"]/$j_price; ?></td>
                <td><?= $ej[$k_penilaian] = $e_cpu + $e_memory + $e_battery + $e_price  ?></td>
                <td><?= $er[$k_penilaian] = $ej[$k_penilaian]/4  ?></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>

            <br>
            <p class="font-weight-bold">Hasil Perhitungan</p>
            <table class="table table-bordered table-hover display">
              <thead class="thead-dark">
                <tr>
                  <th><center>Keterangan</center></th>
                  <th><center>Nilai</center></th>
                </tr>
              </thead>
              <tbody>
                <tr><td>Maks </td>  <td><?= $max = ($j_cpu*$er[0])+($j_memory*$er[1])+($j_battery*$er[2])+($j_price*$er[3]) ?></td></tr>
                <tr><td>CI = Indek konsistensi</td>  <td><?= $ci = ($max-4)/(4-1) ?></td></tr>
                <tr><td>CR = Rasio Konsistensi</td>  <td><?= $cr = $ci/0.9 ?></td></tr>
                <tr>
                  <td class="table-primary">Hasil</td>
                  <td class="table-primary"><?= $cr<0.1 ? 'Konsisten': 'Tidak Konsisten, Silahkan Isi Ulang Perbandingan Kriteria' ?></td>
                </tr>
              </tbody>
            </table>

            
          <form action="<?= base_url("auth_admin/penilaian_hasil_simpan") ?>" method="POST">
            <input name="cpu" type="hidden" value='<?= @$er[0]; ?>' />
            <input name="memory" type="hidden" value='<?= @$er[1]; ?>' />
            <input name="battery" type="hidden" value='<?= @$er[2]; ?>' />
            <input name="price" type="hidden" value='<?= @$er[3]; ?>' />

            <div style="float: right">
              <input type="submit" value="Simpan" class="btn btn-primary">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
