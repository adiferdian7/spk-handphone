

<div class="col-lg-10 mx-auto" style="padding-top: 40px">
    <div class="card">
        <div class="card-header">
            <strong>Edit admin</strong>
        </div>
        <form action="<?= base_url('Auth_admin/saveadmin')  ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Username</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" value="<?= $data['user']?>" name="user" placeholder="Username" class="form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Email</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input name="email" id="text-input" rows="15" value="<?= $data['email']?>" placeholder="Email..." class="form-control"></input>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </form>
    </div>
</div>