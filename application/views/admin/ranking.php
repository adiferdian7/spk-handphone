<div class="card mb-3">
    <div class="card-body">
        <div class="table-responsive">
            <div class="container">
                <div class="row">
                    <div class="col-auto mr-auto">
                        <h3>Form Data Hasil Kriteria</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <div class="panel-body">
                <select id="harga" class="form-control col-sm-3">
                    <option value="1337">Semua harga</option>
                    <option value="1">0 - 3 Juta</option>
                    <option value="2">3 - 6 Juta</option>
                    <option value="3">6 - 10 Juta</option>
                    <option value="4">> 10 Juta</option>
                </select>
                <br />

                <table id="example" class="table table-bordered table-hover display">
                    <thead class="thead-dark">
                        <tr>
                            <th><center>#</center></th>
                            <th>Merk</th>
                            <th>CPU</th>
                            <th>RAM</th>
                            <th>Baterai</th>
                            <th>Harga</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody id="data">
                        <?php
                        $i = 1;
                        foreach ($ranking as $key => $value) {
                            ?>
                            <tr>
                                <td><?= $i++ ?>.</td>
                                <td><?= $value["name"] ?></td>
                                <td><?= $value['cpu'];  ?></td>
                                <td><?= $value['ram'];  ?>Gb</td>
                                <td><?= $value['battery'];  ?>Mah</td>
                                <td>Rp. <?=  number_format($value['harga'], 0, ",", ".");   ?> Rupiah</td>
                                <td><?= $value["value"] ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
    $("#harga").change(function() {
        $.ajax({
            url         : `<?= base_url("auth_admin/ranking_search") ?>/`+$("#harga").val(),
            dataType    : "json",
            type        : "GET",
            beforeSend: function (e) {
                $("#data").empty();
                $("#data").append(`
                    <tr>
                    <td colspan="3">Mohon tunggu...</td>
                    </tr>
                    `);
            },
            success: function(response) {
                $("#data").empty();
                if (response.length > 0) {
                    response.map((data, key) => {
                        $("#data").append(`
                            <tr>
                            <td>${ key+1 }.</td>
                            <td>${ data.value }</td>
                            <td>${ data.name }</td>
                            </tr>
                            `); 
                    });
                }
                else {
                    $("#data").append(`
                        <tr>
                        <td colspan="3">Hasil tidak ditemukan.</td>
                        </tr>
                        `);
                }
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    information = "Not connect (verify network).";
                } else if (jqXHR.status == 404) {
                    information = "Requested page not found.";
                } else if (jqXHR.status == 500) {
                    information = "Internal Server Error.";
                } else if (exception === "parsererror") {
                    information = "Requested JSON parse failed.";
                } else if (exception === "timeout") {
                    information = "Time out error.";
                } else if (exception === "abort") {
                    information = "Ajax request aborted.";
                } else {
                    information = "Uncaught Error ("+jqXHR.responseText+").";
                }
                console.log(information);
            }
        });
    });
</script>