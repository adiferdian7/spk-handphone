<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <div class="clearfix"></div>
        <!-- Orders -->


        <div class="orders">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="box-title">Berita</h4>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th class="serial"></th>
                                            <th>Judul</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <?php foreach ($berita as $key => $value): ?>
                                            <tr>
                                                <td style="width: 1px">
                                                </td><td>
                                                    <?= $value ['judul'] ?>
                                                </td><td>
                                                    <img src="<?= base_url('assets/gambar/berita/'.$value['foto'].'')?>">
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->

        <div class="orders">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">          
                        <div class="card-body">
                            <h4 class="box-title">Ponsel</h4>
                        </div>   
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nama Ponsel</th>
                                            <th>Foto</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <?php foreach ($spesifikasi as $p): ?>
                                            <tr>
                                                <td style="width: 700px;">
                                                    <?= $p['name'] ?>
                                                </td>
                                                <td>
                                                    <img src="<?= base_url(''.$p['foto'].'')?>">
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->
    </div>
    <!-- /#right-panel -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(".table").DataTable({
            "paging":   true,
            "iDisplayLength": 5,
            "bFilter": false,
        });
    </script>