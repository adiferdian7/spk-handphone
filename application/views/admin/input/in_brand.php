

<div class="col-lg-10 mx-auto" style="padding-top: 40px">
    <div class="card">
        <div class="card-header">
            <strong>Input Brand</strong>
        </div>
        <div class="card-body card-block">
            <form action="<?= base_url('auth_admin/sav')  ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Nama</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="nb" placeholder="Nama Brand" class="form-control">
                        <?php if (validation_errors()): ?>
                            <div class="alert-danger">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="file-input" class=" form-control-label">Logo</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="foto" class="form-control-file">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </form>
        </div>
    </div>
</div>