

<div class="col-lg-10 mx-auto" style="padding-top: 40px">
    <div class="card">
        <div class="card-header">
            <strong>Input Spesifikasi Ponsel</strong>
        </div>
        <form action="<?= base_url('Auth_admin/saveberita')  ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Judul</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="judul" autocomplete="off" placeholder="Judul Berita" class="form-control">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="textarea-input" class=" form-control-label">Konten</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <textarea name="isi" id="textarea-input" rows="15" placeholder="Content..." class="form-control"></textarea>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="file-input" class=" form-control-label">File input</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="file" id="file-input" name="foto" class="form-control-file">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Reset
                </button>
            </div>
        </form>
    </div>
</div>