
<div class="col-lg-10 mx-auto" style="padding-top: 40px">
    <div class="card">
        <div class="card-header">
            <strong>Input Spesifikasi Ponsel</strong>
        </div>
        <div class="card-body card-block">
            <form action="<?= base_url('Auth_admin/savespesifikasi')  ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3"><label for="text-input" class=" form-control-label">Nama</label></div>
                    <div class="col-12 col-md-9"><input type="text" id="text-input" name="nama" placeholder="Nama Ponsel" class="form-control" required="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label for="multiple-select" class=" form-control-label">Brand</label>
                    </div>
                    <div class="col col-md-9">
                        <select name="brand" id="multiple-select" multiple="" class="form-control">
                            <?php foreach ($brand as $br): ?>
                                <option style="padding-top: 5px" value="<?= $br['idb']; ?>">
                                    <?= $br['brand']; ?>
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="input" class=" form-control-label">Jenis</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <select name="kategori" id="selectSm" class="form-control-sm form-control">
                            <?php foreach ($jenis as $jn): ?>
                                <option style="padding-top: 5px" value="<?= $jn['idj']; ?>">
                                    <?= $jn['jenis']; ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label class=" form-control-label">Jaringan</label></div>
                            <div class="col col-md-9">
                                <div class="form-check">
                                    <div class="checkbox">
                                        <label for="checkbox1" class="form-check-label ">
                                            <input type="checkbox" id="checkbox1" name="jaringan[]" value="2G" class="form-check-input" checked="checked">2G &nbsp; &nbsp; &nbsp; &nbsp;
                                            <input type="checkbox" id="checkbox1" name="jaringan[]" value="3G" class="form-check-input"checked="checked">3G &nbsp; &nbsp; &nbsp; &nbsp;
                                            <input type="checkbox" id="checkbox1" name="jaringan[]" value="4G" class="form-check-input"checked="checked">4G &nbsp; &nbsp; &nbsp; &nbsp;
                                            <input type="checkbox" id="checkbox1" name="jaringan[]" value="5G" class="form-check-input">5G &nbsp; &nbsp; &nbsp; &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="input" class=" form-control-label">Rilis</label></div>
                            <div class="col-12 col-md-9">
                                <input type="text" id="input" name="tahun" placeholder="Tahun Relese" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="input" class=" form-control-label">Body</label></div>
                            <div class="col-4">
                                <input type="text" id="input-small" name="dimensi" placeholder="Dimensi Body (P x L x T)" class="input-sm form-control-sm form-control">
                            </div> <div class="col-5">
                                <input type="text" id="input-small" name="berat" placeholder="Berat Ponsel (Gram)" class="input-sm form-control-sm form-control">
                            </div>
                            <div class="col col-md-3">
                            </div> <div class="col-4">
                                <select name="bahan" id="selectSm" class="form-control-sm form-control">
                                    <option value="">---- Material Bahan ----</option>
                                    <option value="Rangka Metal dan Kesing Belakang Polycarbonat">Rangka dan Kesing Polycarbonat</option>
                                    <option value="Rangka Metal dan Kesing Belakang Polycarbonat">Rangka Metal dan Kesing Polycarbonat</option>
                                    <option value="Rangka Metal dan Kesing Belakang Polycarbonat">Rangka dan Kesing Metal</option>
                                    <option value="Rangka dan Kesing Belakang Metal ">Rangka Metal dan Kesing Kaca </option>
                                </select>
                            </div> <div class="col-5">
                                <select name="sim" id="selectSm" class="form-control-sm form-control">
                                    <option value="">---- Jumlah Sim Card ----</option>
                                    <option value="Singgle SIM">Singgle Sim</option>
                                    <option value="Dual SIM">Dual Sim</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="input" class=" form-control-label">Display</label>
                            </div>
                            <div class="col-4">
                                <select name="panel" id="selectSm" class="form-control-sm form-control">
                                    <option value="">---- Tipe Panel Layar ----</option>
                                    <option value="IPS">IPS</option>
                                    <option value="AMOLED">AMOLED</option>
                                    <option value="Super AMOLED">Super AMOLED</option>
                                    <option value="Dynamic AMOLED">Dynamic AMOLED</option>
                                </select>
                            </div> 
                            <div class="col-5">
                                <input type="text" id="input-small" name="ukuran" placeholder="Ukuran Diagonal Layar (Inch)" class="input-sm form-control-sm form-control">
                            </div>
                            <div class="col col-md-3">
                            </div>
                            <div class="col-4">
                                <input type="text" id="input-small" name="resolusi" placeholder="Resolusi Layar" class="input-sm form-control-sm form-control">
                            </div>
                            <div class="col-5">
                                <select name="proteksi" id="selectSm" class="form-control-sm form-control">
                                    <option value="">---- Proteksi Layar ----</option>
                                    <option value="Gorila Glass 3">Gorila Glass 3</option>
                                    <option value="Gorila Glass 4">Gorila Glass 4</option>
                                    <option value="Gorila Glass 5">Gorila Glass 5</option>
                                    <option value="Gorila Glass 6">Gorila Glass 6</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="input" class=" form-control-label">Platform</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio1" class="form-check-label ">
                                        <input type="radio" id="inline-radio1" onclick="getOS(1)" name="os" value="android" class="form-check-input" checked>Android &nbsp; &nbsp; &nbsp; &nbsp;
                                    </label>
                                    <label for="inline-radio2" class="form-check-label ">
                                        <input type="radio" id="inline-radio2" onclick="getOS(2)" name="os" value="IOS" class="form-check-input">IOS
                                    </label>
                                </div>
                                <select name="platform" id="selectOS" class="form-control-sm form-control">
                                </select>
                                <div class="form-check-inline form-check">
                                    <label for="inline-radio3" class="form-check-label ">
                                        <input type="radio" id="inline-radio3" name="cpu" value="snapdragon" class="form-check-input cpu">Snapdragon &nbsp; &nbsp; &nbsp; &nbsp;
                                    </label>
                                    <label for="inline-radio4" class="form-check-label ">
                                        <input type="radio" id="inline-radio4" name="cpu" value="mediatek" class="form-check-input cpu">Mediatek &nbsp; &nbsp; &nbsp; &nbsp;
                                    </label>
                                    <label for="inline-radio5" class="form-check-label ">
                                        <input type="radio" id="inline-radio5" name="cpu" value="kirin" class="form-check-input cpu">Kirin &nbsp; &nbsp; &nbsp; &nbsp;
                                    </label>
                                    <label for="inline-radio6" class="form-check-label ">
                                        <input type="radio" id="inline-radio6" name="cpu" value="exinos" class="form-check-input cpu">Exinos &nbsp; &nbsp; &nbsp; &nbsp;
                                    </label>
                                    <input type="radio" id="inline-radio6" name="cpu" value="apple" class="form-check-input cpu">Apple
                                </label>
                            </div>
                            <select name="cpu" id="cpu" class="form-control form-control" required="">
                                <option value=""></option>
                            </select>   
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="input" class=" form-control-label">Memory</label>
                        </div>
                        <div class="col-12 col-md-9">   
                            <div class="form-check">
                            <div style="font-size: 15px; color: grey;">Kapasitas Memory</div>
                                <table>
                                    <tr>
                                        <td style="width: 100px">
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k1" class="form-check-input">16 GB
                                        </td><td style="width: 100px">
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k2" class="form-check-input">32 GB
                                        </td><td style="width: 100px">
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k3" class="form-check-input" checked>64 GB
                                        </td><td style="width: 100px">
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k4" class="form-check-input">128 GB
                                        </td>
                                    </tr><tr>
                                        <td style="width: 100px">
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k5" class="form-check-input">256 GB
                                        </td><td style="width: 100px">
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k6" class="form-check-input">512 GB
                                        </td> <td style="width: 100px"    >
                                            <input type="checkbox" id="checkbox1" name="memory[]" value="k7" class="form-check-input">1 GB
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <hr>

                            <div class="form-check">
                            <div style="font-size: 15px; color: grey;">Kapasitas RAM</div>
                                <table>
                                    <tr>
                                        <td style="width: 100px">
                                            <input type="radio" id="checkbox1" name="ram" value="1" class="form-check-input">1 GB
                                        </td><td style="width: 100px">
                                            <input type="radio" id="checkbox1" name="ram" value="2" class="form-check-input">2 GB
                                        </td><td style="width: 100px">
                                            <input type="radio" id="checkbox1" name="ram" value="3" class="form-check-input">3 GB
                                        </td> <td style="width: 100px">
                                            <input type="radio" id="checkbox1" name="ram" value="4" class="form-check-input" checked>4 GB
                                        </td>
                                    </tr><tr>
                                        <td style="width: 100px">
                                            <input type="radio" id="checkbox1" name="ram" value="6" class="form-check-input">6 GB
                                        </td><td style="width: 100px">
                                            <input type="radio" id="checkbox1" name="ram" value="8" class="form-check-input">8 GB
                                        </td><td>
                                            <input type="radio" id="checkbox1" name="ram" value="12" class="form-check-input">12 GB
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <hr>

                            <select name="slot" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Slot Memory Card</div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="input" class=" form-control-label">Kamera</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="utama" id="selectSm" class="form-control-sm form-control">
                                <option value="Single Camera">Single Camera</option>
                                <option value="Dual Camera">Dual Camera</option>
                                <option value="Triple Camera">Triple Camera</option>
                                <option value="Quad Camera">Quad Camera</option>
                                <option value="Penta Camera">Penta Camera</option>
                                <option value="Hexa Camera">Hexa Camera</option>
                            </select>
                            <div style="font-size: 15px; color: grey; padding-left: 10px;">Kamera Belakang</div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="input" class=" form-control-label"></label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="selfi" id="selectSm" class="form-control-sm form-control">
                                <option value="Single Camera">Single Camera</option>
                                <option value="Dual Camera">Dual Camera</option>
                                <option value="Triple Camera">Triple Camera</option>
                            </select>
                            <div style="font-size: 15px; color: grey; padding-left: 10px;">Kamera Depan</div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="input" class=" form-control-label">Sounds</label>
                        </div>
                        <div class="col-4">
                            <select name="suara" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Loundspeker</div>
                        </div>
                        <div class="col-5">
                            <select name="jack" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">3.5mm Jack Audio</div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="input" class=" form-control-label">Fitur Umum</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <select name="wifi" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">WLAN</div>
                        </div>
                        <div class="col-12 col-md-5">
                            <select name="bluetooth" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Bluetooth</div>
                        </div>
                        <div class="col col-md-3">
                        </div>
                        <div class="col-12 col-md-4">
                            <select name="gps" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Integrated GPS</div>
                        </div>
                        <div class="col-12 col-md-5">
                            <select name="nfc" id="selectSm" class="form-control-sm form-control">
                                <option value="0" selected>No</option>
                                <option value="1">Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">NFC</div>
                        </div>
                        <div class="col col-md-3">
                        </div>
                        <div class="col-12 col-md-4">
                            <select name="infrared" id="selectSm" class="form-control-sm form-control">
                                <option value="0" selected>No</option>
                                <option value="1">Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Infrared Blaster</div>
                        </div>
                        <div class="col-12 col-md-5">
                            <select name="radio" id="selectSm" class="form-control-sm form-control">
                                <option value="0">No</option>
                                <option value="1" selected>Yes</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Radio</div>
                        </div>
                        <div class="col col-md-3">
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="usb" id="selectSm" class="form-control-sm form-control">
                                <option value="0" selected>Tipe micro B</option>
                                <option value="1">Tipe micro C</option>
                                <option value="2">Lightning</option>
                            </select>
                            <div style="font-size: 12px; color: grey; padding-left: 10px;">Tipe USB</div>
                            <hr>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="input" class=" form-control-label">Fitur Tambahan</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="form-check">
                                <table>
                                    <tr>
                                        <td style="width: 170px">
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s1" class="form-check-input"checked="checked">Fingerprint
                                        </td style="width: 170px"><td>
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s2" class="form-check-input"checked="checked">Barometer
                                        </td style="width: 170px"><td>
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s4" class="form-check-input"checked="checked">Compas
                                        </td>
                                    </tr><tr>
                                        <td style="width: 170px">
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s5" class="form-check-input"checked="checked">Proximity
                                        </td>
                                        <td style="width: 170px">
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s8" class="form-check-input">Wireless Charging
                                        </td>
                                        <td style="width: 170px">
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s6" class="form-check-input"checked="checked">Accelerometer
                                        </td>
                                    </tr><tr>
                                        <td>
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s7" class="form-check-input">Face ID
                                        </td><td>
                                            <input type="checkbox" id="checkbox1" name="tambahan[]" value="s3" class="form-check-input">Gyro
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <input type="text" id="input-small" name="lainya" placeholder="Fitur Tambahan Lainya" class="input-sm form-control-sm form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="input" class=" form-control-label">Baterai</label>
                        </div>

                        <div class="col-4">
                            <input type="text" id="input-small" name="baterai" placeholder="Ukuran Baterai" class="input-sm form-control-sm form-control" required="">
                        </div>
                        <div class="col-5">
                            <select name="qc" id="selectSm" class="form-control-sm form-control">
                                <option value="Tidak Ada Pengisian Daya Cepat">---- Fast Carging ----</option>
                                <option value="12 Watt Fast Charging">10 W</option>
                                <option value="12 Watt Fast Charging">12 W</option>
                                <option value="15 Watt Fast Charging">15 W</option>
                                <option value="18 Watt Fast Charging">18 W</option>
                                <option value="25 Watt Fast Charging">25 W</option>
                                <option value="30 Watt Fast Charging">30 W</option>
                                <option value="40 Watt Fast Charging">40 W</option>
                                <option value="50 Watt Fast Charging">50 W</option>
                                <option value="60 Watt Fast Charging">60 W</option>
                                <option value="100 Watt Fast Charging">100 W</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="input" class=" form-control-label">Harga</label></div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="input" name="harga" placeholder="Harga Ponsel (IDR)" class="form-control" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="file-multiple-input" class=" form-control-label">Input Foto</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="file" id="file-multiple-input" name="foto" multiple="" class="form-control-file"></div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <script type="text/javascript">
            var defaultOS = `<option value="Jelly Bean">Jelly Bean</option>
            <option value="Kit Kat">Kit Kat</option>
            <option value="Lolipop">Lolipop</option>
            <option value="Marsmellow">Marsmellow</option>
            <option value="Nougat">Nougat</option>
            <option value="Oreo">Oreo</option>
            <option value="Pie">Pie</option>
            <option value="Android 10">Android 10</option>`;

            $("#selectOS").html(defaultOS)

            function getOS(id){
                var html = '';
                if(id==1){
                    html = defaultOS;
                }else{
                    html = `<option value="IOS 9">IOS 9</option>
                    <option value="IOS 10">IOS 10</option>
                    <option value="IOS 11">IOS 11</option>
                    <option value="IOS 12">IOS 12</option>
                    <option value="IOS 13">IOS 13</option>`
                }
                $("#selectOS").html(html)

            }
            $(document).ready(function(){
                var CPUsnapdragon = `<option value="Snapdragon 425">Snapdragon 425</option>
                <option value="Snapdragon 427">Snapdragon 427</option>
                <option value="Snapdragon 430">Snapdragon 430</option>
                <option value="Snapdragon 450">Snapdragon 450</option>
                <option value="Snapdragon 460">Snapdragon 460</option>
                <option value="Snapdragon 625">Snapdragon 625</option>
                <option value="Snapdragon 630">Snapdragon 630</option>
                <option value="Snapdragon 636">Snapdragon 636</option>
                <option value="Snapdragon 660">Snapdragon 660</option>
                <option value="Snapdragon 665">Snapdragon 665</option>
                <option value="Snapdragon 670">Snapdragon 670</option>
                <option value="Snapdragon 710">Snapdragon 700</option>
                <option value="Snapdragon 712">Snapdragon 712</option>
                <option value="Snapdragon 720">Snapdragon 720</option>
                <option value="Snapdragon 730">Snapdragon 730</option>
                <option value="Snapdragon 765">Snapdragon 765</option>
                <option value="Snapdragon 821">Snapdragon 821</option>
                <option value="Snapdragon 835">Snapdragon 835</option>
                <option value="Snapdragon 845">Snapdragon 845</option>
                <option value="Snapdragon 855">Snapdragon 855</option>
                <option value="Snapdragon 865">Snapdragon 865</option>`;

                var CPUmediatek = `<option value="Mediatek Helio P30">Mediatek Helio P30</option>
                <option value="Mediatek Helio P35">Mediatek Helio P35</option>
                <option value="Mediatek Helio P60">Mediatek Helio P60</option>
                <option value="Mediatek Helio P65">Mediatek Helio P65</option>
                <option value="Mediatek Helio P70">Mediatek Helio P70</option>
                <option value="Mediatek Helio P90">Mediatek Helio P90</option>
                <option value="Mediatek Helio G90">Mediatek Helio G90</option>
                <option value="Mediatek Helio G90T">Mediatek Helio G90T</option>
                <option value="Mediatek Helio X10">Mediatek Helio X10</option>
                <option value="Mediatek Helio X20">Mediatek Helio X20</option>
                <option value="Mediatek Helio X23">Mediatek Helio X23</option>
                <option value="Mediatek Helio X25">Mediatek Helio X25</option>
                <option value="Mediatek Helio X27">Mediatek Helio X27</option>
                <option value="Mediatek Helio X30">Mediatek Helio X30</option>`;

                var CPUkirin = `<option value="Kirin 650">Kirin 650</option>
                <option value="Kirin 655">Kirin 655</option>
                <option value="Kirin 658">Kirin 658</option>
                <option value="Kirin 659">Kirin 659</option>
                <option value="Kirin 810">Kirin 710</option>
                <option value="Kirin 810">Kirin 810</option>
                <option value="Kirin 910">Kirin 910</option>
                <option value="Kirin 910T">Kirin 910T</option>
                <option value="Kirin 920">Kirin 920</option>
                <option value="Kirin 925">Kirin 925</option>
                <option value="Kirin 928">Kirin 928</option>
                <option value="Kirin 930">Kirin 930</option>
                <option value="Kirin 935">Kirin 935</option>
                <option value="Kirin 950">Kirin 950</option>
                <option value="Kirin 955">Kirin 955</option>
                <option value="Kirin 960">Kirin 960</option>
                <option value="Kirin 970">Kirin 970</option>
                <option value="Kirin 980">Kirin 980</option>
                <option value="Kirin 990">Kirin 990</option>`;

                var CPUexinos = `<option value="Exynos 8 Octa 8890">Exynos 8 Octa 8890</option>
                <option value="Exynos 8895">Exynos 8895</option>
                <option value="Exynos 9810">Exynos 9810</option>
                <option value="Exynos 9820">Exynos 9820</option>
                <option value="Exynos 9825">Exynos 9825</option>
                <option value="Exynos 980">Exynos 980</option>
                <option value="Exynos 990">Exynos 990</option>`;

                var CPUapple = `<option value="Apple A9">Apple A9</option>
                <option value="Apple A10 Fusion">Apple A10 Fusion</option>
                <option value="Apple A10X Fusion">Apple A10X Fusion</option>
                <option value="Apple A11 Bionic">Apple A11 Bionic</option>
                <option value="Apple A12 Bionic">Apple A12 Bionic</option>
                <option value="Apple A12X Bionic">Apple A12X Bionic</option>
                <option value="Apple A13 Bionic">Apple A13 Bionic</option>`;

                var cpu;

                $( ".cpu" ).click(function() {
                    var cpu = $("input[name='cpu']:checked").val();
                    switch (cpu) {
                        case 'snapdragon':
                        $('#cpu').html();
                        $('#cpu').html(CPUsnapdragon);
                        break;

                        case 'mediatek':
                        $('#cpu').html();
                        $('#cpu').html(CPUmediatek);
                        break;

                        case 'kirin':
                        $('#cpu').html();
                        $('#cpu').html(CPUkirin);
                        break;

                        case 'exinos':
                        $('#cpu').html();
                        $('#cpu').html(CPUexinos);
                        break;

                        case 'apple':
                        $('#cpu').html();
                        $('#cpu').html(CPUapple);
                        break;

                        case 'exinos':
                        $('#cpu').html();
                    } 
                });
            }); 

        </script>

