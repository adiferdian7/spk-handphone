           
<div class="col-lg-10 mx-auto" style="padding-top: 40px; padding-bottom: 40px;">
    <div class="card">
        <div class="card-header">
            <strong>Input Akun User</strong>
        </div>
        <div class="card-body card-block">
            <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Username</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="user" placeholder="Nama" class="form-control">
                        <?php if (validation_errors()): ?>
                            <div class="alert-danger">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Password</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="Password" id="text-input" name="pass" placeholder="Password" class="form-control">
                        <?php if (validation_errors()): ?>
                            <div class="alert-danger">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">E-mail</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="text" id="text-input" name="email" placeholder="E-mail" class="form-control">
                        <?php if (validation_errors()): ?>
                            <div class="alert-danger">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </form>
            </div>
        </div>
    </div>