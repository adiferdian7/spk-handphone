
<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">                
        <div class="clearfix"></div>
        <!-- Orders -->
        <div class="orders">
            <div class="row">
                <div class="col-xl-12 mx-auto">
                    <div class="card">
                        <!-- Orders -->
                        <div class="breadcrumbs-inner">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="page-header float-left">
                                        <div class="card-header" style="max-height: 60px">
                                            <h1>Brand</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="page-header float-right">
                                        <div class="page-title">
                                            <ol class="breadcrumb text-right">
                                                <a href="<?= base_url('auth_admin/inbr')  ?>">
                                                    <button type="button" class="btn btn-success">Input</button></a>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table" id="table">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Nama</th>
                                            <th>Logo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <?php foreach ($brand as $p): ?>
                                            <tr>
                                                <td style="width: 40px">

                                                </td>
                                                <td style="width: 200px;">
                                                    <?= $p['brand'] ?>
                                                </td>
                                                <td>
                                                    <img src="<?= base_url('assets/gambar/logo/'.$p['foto'].'')?>">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-primary">Edit</button>
                                                    <a href="<?= base_url('auth_admin/hapusbrand/').$p['idb']?>" class="btn btn-danger">Hapus</a>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->
    </div>
    <!-- .animated -->
</div>
<!-- /#right-panel -->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $("#table").DataTable({
        "paging":   true,
        "iDisplayLength": 6,
        "bFilter": false,
    });
</script>