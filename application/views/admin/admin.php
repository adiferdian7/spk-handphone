
<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">                
        <div class="clearfix"></div>

        <div class="orders">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <!-- Orders -->
                        <div class="breadcrumbs-inner">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="page-header float-left">
                                        <div class="card-header" style="max-height: 60px">
                                            <h1>List Admin</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="page-header float-right">
                                        <div class="page-title">
                                            <ol class="breadcrumb text-right">
                                                <a href="<?= base_url('auth_admin/ina')  ?>"><button type="button" class="btn btn-success">Input</button></a>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table ">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Username</th>
                                            <th>E-mail</th>
                                            <th>Telfon</th>
                                            <th></th>
                                        </thead>
                                        <?php foreach ( $admin as $ad ): ?>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span class="name">
                                                            <?= $ad['ida']  ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span class="name">
                                                            <?= $ad['username']  ?>
                                                        </span>
                                                    </td>
                                                    <td> 
                                                        <span class="product">
                                                            <?= $ad['email']  ?>
                                                        </span>
                                                    </td>
                                                    <td> 
                                                        <span class="product">
                                                            <?= $ad['telefon']  ?>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="<?=base_url('auth_admin/edadmin?id=').$ad['ida']?>" type="button" class="btn btn-primary">Edit</a>
                                                        <button type="button" class="btn btn-danger">Hapus</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        <?php endforeach ?>
                                    </table>
                                </div> <!-- /.table-stats -->
                            </div>
                        </div> <!-- /.card -->
                    </div>  <!-- /.col-lg-8 -->
                </div>
            </div>
            <!-- /.orders -->
        </div>
        <!-- .animated -->
    </div>
</div>
    <!-- /#right-panel -->