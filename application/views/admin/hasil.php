<div class="card mb-3">
    <div class="card-body">
        <div class="table-responsive">
            <div class="container">
                <div class="row">
                    <div class="col-auto mr-auto">
                        <h3>Form Data Hasil Kriteria</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <div class="panel-body">
                <form method="POST" action="<?= base_url("auth_admin/ranking") ?>">
                    <table id="example" class="table table-bordered table-hover display">
                        <thead class="thead-dark">
                            <tr>
                                <th><center>Kriteria</center></th>
                                <th><center>CPU</center></th>
                                <th><center>Memory</center></th>
                                <th><center>Battery</center></th>
                                <th><center>Price</center></th>
                            </tr>
                        </thead>
                        <?php
                        foreach ($spesifikasi as $spe_data) {
                            ?>
                            <tr>
                                <td><?= $spe_data["name"] ?></td>
                                <td><?= $spe_data["cpu"] ?></td>
                                <td><?= $spe_data["memory"] ?></td>
                                <td><?= $spe_data["battery"] ?></td>
                                <td><?= $spe_data["price"] ?></td>
                            </tr>
                            <?php
                            @$j_cpu     += $spe_data["cpu"];
                            @$j_memory  += $spe_data["memory"];
                            @$j_battery += $spe_data["battery"];
                            @$j_price   += $spe_data["price"];
                        }
                        ?>
                        <tbody>
                        </tbody>
                    </table>

                    <table id="example" class="table table-bordered table-hover display">
                        <thead class="thead-dark">
                            <tr>
                                <th><center>Kriteria</center></th>
                                <th><center>CPU</center></th>
                                <th><center>Memory</center></th>
                                <th><center>Battery</center></th>
                                <th><center>Price</center></th>
                            </tr>
                        </thead>
                        <?php
                        foreach ($spesifikasi as $spe_key => $spe_data) {
                            ?>
                            <tr>
                                <td><?= $spe_data["name"] ?></td>
                                <td><?= $norm[$spe_key][1]     = $spe_data["cpu"]/$j_cpu ?></td>
                                <td><?= $norm[$spe_key][2]  = $spe_data["memory"]/$j_memory ?></td>
                                <td><?= $norm[$spe_key][3] = $spe_data["battery"]/$j_battery ?></td>
                                <td><?= $norm[$spe_key][4]   = $spe_data["price"]/$j_price ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tbody>
                        </tbody>
                    </table>

                    <table id="example" class="table table-bordered table-hover display">
                        <thead class="thead-dark">
                            <tr>
                                <th><center>Kriteria</center></th>
                                <th><center>CPU</center></th>
                                <th><center>Memory</center></th>
                                <th><center>Battery</center></th>
                                <th><center>Price</center></th>
                            </tr>
                        </thead>
                        <?php
                        foreach ($spesifikasi as $spe_key => $spe_data) {
                            ?>
                            <tr>
                                <td><?= $spe_data["name"] ?></td>
                                <td><?= $rekom[$spe_key][1] = $norm[$spe_key][1]*$rerata[0]["rerata"] ?></td>
                                <td><?= $rekom[$spe_key][2] = $norm[$spe_key][2]*$rerata[1]["rerata"] ?></td>
                                <td><?= $rekom[$spe_key][3] = $norm[$spe_key][3]*$rerata[2]["rerata"] ?></td>
                                <td><?= $rekom[$spe_key][4] = $norm[$spe_key][4]*$rerata[3]["rerata"] ?></td>
                            </tr>
                            <?php
                            echo "<input name=\"rank[".$spe_data["id"]."][telephone_id]\" value=\"".$spe_data["spesifikasi_id"]."\" type=\"hidden\" />";
                            echo "<input name=\"rank[".$spe_data["id"]."][value]\" value=\"".array_sum($rekom[$spe_key])."\" type=\"hidden\" />";
                        }
                        ?>
                        <tbody>
                        </tbody>
                    </table>

                    <div class="text-center">
                        <input type="submit" value="Lihat Ranking" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>