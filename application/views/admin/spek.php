

<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">                
        <div class="clearfix"></div>
        <!-- Orders -->
        <div class="orders">
            <div class="row">                   
                <div class="col-xl-12 mx-auto">
                    <div class="card">
                        <!-- Orders -->
                        <div class="breadcrumbs-inner">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="page-header float-left">
                                        <div class="card-header" style="max-height: 60px">
                                            <h1>Ponsel</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body--">
                            <div class="table-stats order-table ov-h">
                                <table class="table" id="table">
                                    <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Nama Ponsel</th>
                                            <th>Foto</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($spesifikasi as $p): ?>
                                            <tr>
                                                <td style="width: 40px">
                                                </td>
                                                <td style="width: 700px;">
                                                    <?= $p['name'] ?>
                                                </td>
                                                <td>
                                                    <img src="<?= base_url(''.$p['foto'].'')?>">
                                                </td>
                                                <td>
                                                    <a href="<?=base_url('auth_admin/editspek/').$p['ids']?>" type="button" class="btn btn-primary">Edit</a>
                                                    <a href="<?=base_url('auth_admin/hapuspek/').$p['ids']?>" type="button" class="btn btn-danger">Hapus</a>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div> <!-- /.table-stats -->
                        </div>
                    </div> <!-- /.card -->
                </div>  <!-- /.col-lg-8 -->
            </div>
        </div>
        <!-- /.orders -->
    </div>
    <!-- .animated -->
</div>
<!-- /#right-panel -->


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    $("#table").DataTable({
        "paging":   true,
        "iDisplayLength": 6,
        "bFilter": false,
    });
</script>