<div class="card mb-3">

  <div class="card-body">
    <div class="table-responsive">

      <div class="container">

        <form class="form-horizontal style-form" style="margin-top: 10px;" action="<?= base_url("auth_admin/penilaian_simpan") ?>" method="post" enctype="multipart/form-data" name="form1" id="form1">
          <div class="alert alert-primary" role="alert">
            <h6>Keterangan Nilai:</h6>
            <ul>
              <li>1. Kedua elemen sama penting</li>
              <li>3. Elemen yang satu sedikit lebih penting dari elemen lainnya</li>
              <li>5. Elemen yang satu lebih penting dari elemen lainnya</li>
              <li>7. Elemen yang satu sangat penting dari elemen lainnya</li>
              <li>9. Elemen yang satu mutlak sangat penting dari elemen lainnya</li>
              <li>2.4.6.8 Nilai antaradua nilai pertimabangan yang berdekatan</li>
            </ul>
          </div>

          <table id="example" class="table table-hover table-bordered">
            <thead>
              <tr>
                <th><center>Kriteria <i class="fa fa-sort"></i></center></th>
                <th><center>CPU <i class="fa fa-sort"></i></center></th>
                <th><center>Memory <i class="fa fa-sort"></i></center></th>
                <th><center>Baterai <i class="fa fa-sort"></i></center></th>
                <th><center>Harga <i class="fa fa-sort"></i></center></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><input name="luas" type="text" readonly="" class="form-control"  value='CPU' required /></td>
                <td><input name="c[1][1]" type="text" readonly="" class="form-control" value="1" required /></td>
                <td>
                  <select class="form-control" required="" onchange='sum();' name="c[1][2]" id='c12'>
                    <option value=''></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                  </select>
                </td>
                <td> <select class="form-control" required="" onchange='sum();' name="c[1][3]" id='c13'>
                  <option value=''></option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                </select></td>
                <td> <select class="form-control" required="" onchange='sum();' name="c[1][4]" id='c14'>
                  <option value=''></option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                </tr>

                <tr>
                  <td><input name="luas" type="text" readonly="" class="form-control"  value='Memory' required /></td>
                  <td><input name="c[2][1]" id="c21" type="text" readonly="" class="form-control"  required /></td>
                  <td>
                    <input name="c[2][2]" id="c22" type="text" readonly="" class="form-control"  required value="1" />
                  </td>
                  <td> <select class="form-control" required="" onchange='sum();' name="c[2][3]" id='c23'>
                    <option value=''></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                  </select></td>
                  <td> <select class="form-control" required="" onchange='sum();' name="c[2][4]" id='c24'>
                    <option value=''></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                  </tr>
                  <tr>
                    <td><input name="luas" type="text" readonly="" class="form-control"  value='Baterai' required /></td>
                    <td><input name="c[3][1]" id="c31" type="text" readonly="" class="form-control"  required /></td>
                    <td>
                      <input name="c[3][2]" id="c32" type="text" readonly="" class="form-control"  required />
                    </td>
                    <td> <input name="c[3][3]" id="c33" type="text" readonly="" class="form-control" value="1" required /></td>
                    <td><select class="form-control" required="" onchange='sum();' name="c[3][4]" id='c34'>
                      <option value=''></option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                    </tr>
                    <tr>
                      <td><input name="luas" type="text" readonly="" class="form-control"  value='Harga' required /></td>
                      <td><input name="c[4][1]" id='c41' type="text" readonly="" class="form-control"  required /></td>
                      <td>
                        <input name="c[4][2]" id='c42' type="text" readonly="" class="form-control" required />
                      </td>
                      <td> <input name="c[4][3]" id='c43' type="text" readonly="" class="form-control"  required /></td>
                      <td> <input name="c[4][4]" id='c44' type="text" readonly="" class="form-control" value="1" required /></td>
                    </tr>

                  </tbody>
                </table>

                <div class="form-group" style="margin-bottom: 20px;">
                  <label class="col-sm-2 col-sm-2 control-label"></label>
                  <div style="float: right">
                      <input type="submit" value="Hitung" class="btn btn-primary" />
                  </div>

                </div>

              </form>


            </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">
      function sum() {
        var txtFirstNumberValue = document.getElementById('c12').value;
        var txtSecondNumberValue = 1;
        var result = parseInt(txtSecondNumberValue)/parseInt(txtFirstNumberValue)  ;
        if (!isNaN(result)) {
          document.getElementById('c21').value = result;
        }

        var txtFirstNumberValue = document.getElementById('c13').value;
        var txtSecondNumberValue = 1;
        var result = parseInt(txtSecondNumberValue)/parseInt(txtFirstNumberValue)  ;
        if (!isNaN(result)) {
          document.getElementById('c31').value = result;
        }
        var txtFirstNumberValue = document.getElementById('c14').value;
        var txtSecondNumberValue = 1;
        var result = parseInt(txtSecondNumberValue)/parseInt(txtFirstNumberValue)  ;
        if (!isNaN(result)) {
          document.getElementById('c41').value = result;
        }

        var txtFirstNumberValue = document.getElementById('c23').value;
        var txtSecondNumberValue = 1;
        var result = parseInt(txtSecondNumberValue)/parseInt(txtFirstNumberValue)  ;
        if (!isNaN(result)) {
          document.getElementById('c32').value = result;
        }
        var txtFirstNumberValue = document.getElementById('c24').value;
        var txtSecondNumberValue = 1;
        var result = parseInt(txtSecondNumberValue)/parseInt(txtFirstNumberValue)  ;
        if (!isNaN(result)) {
          document.getElementById('c42').value = result;
        }

        var txtFirstNumberValue = document.getElementById('c34').value;
        var txtSecondNumberValue = 1;
        var result = parseInt(txtSecondNumberValue)/parseInt(txtFirstNumberValue)  ;
        if (!isNaN(result)) {
          document.getElementById('c43').value = result;
        }

      }

      </script>
