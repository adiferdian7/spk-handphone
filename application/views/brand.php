

<!-- START PAGE CONTENT -->
<section id="page-content" class="page-wrapper section">

    <!-- PRODUCT TAB SECTION START -->
    <div class="product-tab-section pt-30 pb-55">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title text-left mb-20">
                        <h2 class="uppercase">Daftar Brand/ Merek Ponsel</h2>
                    </div>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- UP COMMING PRODUCT SECTION START -->
                <div class="up-comming-product-section ptb-20">
                    <div class="container">
                        <div class="row">
                            <!-- up-comming-pro -->
                            <div class="col-lg-12">
                                <div class="up-comming-pro1 gray-bg up-comming-pro-2 clearfix">
                                 <table class="table" style="height: 500px;">
                                    <?php 

                                    foreach ($brand as $key => $value){
                                        if($key%4 == 0)
                                        {
                                            ?>
                                            <tr>
                                                <td>
                                                    <center><h4><a href="<?=base_url('auth/list/').$value['idb'] ?>" class="btn-hover-2"><?=$value['brand']?></a></h4></center>
                                                </td>
                                                <?php
                                            }else{
                                                ?>
                                                <td>
                                                    <center><h4><a href="<?=base_url('auth/list/').$value['idb'] ?>" class="btn-hover-2"><?=$value['brand']?></a></h4></center>
                                                </td>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- UP COMMING PRODUCT SECTION END -->
            </div>
        </div>
    </div>
    <!-- PRODUCT TAB SECTION END -->
</div>
    <!-- Body main wrapper end -->