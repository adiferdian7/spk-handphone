
        <!-- Start page content -->
        <div id="page-content" class="page-wrapper section" style="padding-top: 30px;">

            <!-- LOGIN SECTION START -->
            <div class="login-section mb-80" >
                <div class="container">
                    <div class="row">
                        <!-- new-customers -->
                        <div class="col-lg-6 mx-auto">
                            <div class="new-customers">
                                <form action="#">
                                    <h6 class="widget-title border-left mb-50">Akun Baru</h6>
                                    <div class="login-account p-30 box-shadow">
                                        <h4>Nama</h4>
                                        <input type="text"  placeholder="Masukan Nama">
                                        <h4>Nomer Telfon</h4>
                                        <input type="text"  placeholder="Masukan Nomer Telfon">
                                        <h4>E-mail</h4>
                                        <input type="text"  placeholder="Email address here...">
                                        <h4>Password</h4>
                                        <input type="password"  placeholder="Password">
                                        <input type="password"  placeholder="Confirm Password">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button class="submit-btn-1 mt-20 btn-hover-1" type="submit" value="register">Register</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="reset">Clear</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- LOGIN SECTION END -->             

        </div>
        <!-- End page content -->